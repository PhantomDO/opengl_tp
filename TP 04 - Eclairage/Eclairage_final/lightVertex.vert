#version 330 core
layout (location = 0) in vec3 position;

uniform mat4 proj;
uniform mat4 model;
uniform mat4 view;

struct sLight
{
	vec3 position;
	vec3 ambiant;
	vec3 diffus;
	vec3 specular;
};

uniform sLight light;

void main()
{
	mat4 pos = mat4(1); // matrice identit�
	pos[3].xyz = light.position; // assigne x y z
	gl_Position = proj * pos * model * vec4(position, 1.0);
}
