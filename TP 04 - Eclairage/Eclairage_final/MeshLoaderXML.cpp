#include "stdafx.h"
#include "MeshLoaderXML.h"
#include <fstream>
#include <sstream>
#include <map>
#include <functional>
#include <iostream>
#include <regex>
using namespace std;


//Nested CSubMeshes class
CMeshLoaderXML::CSubMeshes::CSubMeshes(CSubMeshes&& submeshes)
  : m_vMeshesNbIndices( std::move(submeshes.m_vMeshesNbIndices) )
  , m_vMeshesStartByteOffsets( std::move(submeshes.m_vMeshesStartByteOffsets) )
  , m_vTextureFiles( std::move(submeshes.m_vTextureFiles) )
  , m_bSameTexture( submeshes.m_bSameTexture )
{
}

void CMeshLoaderXML::CSubMeshes::addMesh(GLsizei nNbIndices, const std::string& strTextureFile)
{
  if(m_vMeshesNbIndices.size() == 0)
    m_vMeshesStartByteOffsets.push_back(0);
  else
    m_vMeshesStartByteOffsets.push_back(reinterpret_cast<GLvoid*>(reinterpret_cast<size_t>(m_vMeshesStartByteOffsets.back()) + m_vMeshesNbIndices.back()*sizeof(GLuint)));
  m_vMeshesNbIndices.push_back(nNbIndices);
  if(m_vTextureFiles.size() != 0 && m_vTextureFiles.back().compare(strTextureFile) != 0)
    m_bSameTexture = false;
  m_vTextureFiles.push_back(strTextureFile);
}

void CMeshLoaderXML::CSubMeshes::loadFromBinFile(std::ifstream& file)
{
  size_t nBufSize;
  file.read(reinterpret_cast<char*>(&nBufSize), sizeof(nBufSize));
  m_vMeshesNbIndices.resize(nBufSize);
  m_vMeshesStartByteOffsets.resize(nBufSize);
  m_vTextureFiles.resize(nBufSize);
  for(GLsizei& nVal : m_vMeshesNbIndices)
    file.read(reinterpret_cast<char*>(&nVal), sizeof(nVal));
  for(GLvoid*& pVal : m_vMeshesStartByteOffsets)
    file.read(reinterpret_cast<char*>(&pVal), sizeof(pVal));
  for(size_t i = 0; i < nBufSize; ++i)
  {
    size_t nStrLen;
    file.read(reinterpret_cast<char*>(&nStrLen), sizeof(nStrLen));
    m_vTextureFiles[i].resize(nStrLen);
    for(size_t j = 0; j < nStrLen; ++j)
      file.read(reinterpret_cast<char*>(&m_vTextureFiles[i][j]), sizeof(string::value_type));
    if(i >= 1)
      if(m_vTextureFiles[i].compare(m_vTextureFiles[i - 1]) != 0)
        m_bSameTexture = false;
  }
}

void CMeshLoaderXML::CSubMeshes::saveToBinFile(std::ofstream& file) const
{
  size_t nBufSize = m_vMeshesNbIndices.size();
  file.write(reinterpret_cast<const char*>(&nBufSize), sizeof(nBufSize));
  for(const GLsizei& nVal : m_vMeshesNbIndices)
    file.write(reinterpret_cast<const char*>(&nVal), sizeof(nVal));
  for(const GLvoid* const& pVal : m_vMeshesStartByteOffsets)
    file.write(reinterpret_cast<const char*>(&pVal), sizeof(pVal));
  for(const string& strTexture : m_vTextureFiles)
  {
    nBufSize = strTexture.size();
    file.write(reinterpret_cast<const char*>(&nBufSize), sizeof(nBufSize));
    file.write(reinterpret_cast<const char*>(strTexture.data()), nBufSize*sizeof(string::value_type));
  }
}

void CMeshLoaderXML::CSubMeshes::clear()
{
  m_vMeshesNbIndices.clear();
  m_vMeshesStartByteOffsets.clear();
  m_vTextureFiles.clear();
  m_bSameTexture = true;
}

CMeshLoaderXML::CMeshLoaderXML()
  : m_nNbPositionComponents(0)
  , m_nNbNormalComponents(0)
  , m_nNbColorComponents(0)
  , m_nNbTexCoordComponents(0)
  , m_nPositionOffset(0)
  , m_nNormalOffset(0)
  , m_nColorOffset(0)
  , m_nTexCoordOffset(0)
  , m_ePrimitives(GL_POINTS)
{
}


CMeshLoaderXML::CMeshLoaderXML(CMeshLoaderXML&& mesh)
  : m_nNbPositionComponents(mesh.m_nNbPositionComponents)
  , m_nNbNormalComponents(mesh.m_nNbNormalComponents)
  , m_nNbColorComponents(mesh.m_nNbColorComponents)
  , m_nNbTexCoordComponents(mesh.m_nNbTexCoordComponents)
  , m_nPositionOffset(mesh.m_nPositionOffset)
  , m_nNormalOffset(mesh.m_nNormalOffset)
  , m_nColorOffset(mesh.m_nColorOffset)
  , m_nTexCoordOffset(mesh.m_nTexCoordOffset)
  , m_ePrimitives(mesh.m_ePrimitives)

  , m_BoundingBox(mesh.m_BoundingBox)

  , m_subMeshes(std::move(mesh.m_subMeshes))

  , m_vVBO(std::move(mesh.m_vVBO))
  , m_vEBO(std::move(mesh.m_vEBO))
{
  mesh.m_nNbPositionComponents = (0);
  mesh.m_nNbNormalComponents = (0);
  mesh.m_nNbColorComponents = (0);
  mesh.m_nNbTexCoordComponents = (0);
  mesh.m_nPositionOffset = (0);
  mesh.m_nNormalOffset = (0);
  mesh.m_nColorOffset = (0);
  mesh.m_nTexCoordOffset = (0);
  mesh.m_ePrimitives = (GL_POINTS);
}

bool CMeshLoaderXML::loadFromFile(const std::string& strFileName)
{
  //réinitilisation de l'objet
  m_vVBO.clear();
  m_vEBO.clear();
  m_nNbPositionComponents = 0;
  m_nNbNormalComponents = 0;
  m_nNbColorComponents = 0;
  m_nNbTexCoordComponents = 0;
  m_nPositionOffset = 0;
  m_nNormalOffset = 0;
  m_nColorOffset = 0;
  m_nTexCoordOffset = 0;
  m_ePrimitives = GL_POINTS;
  m_BoundingBox = { {0}, {0} };
  m_subMeshes.clear();

  cout << "Chargement du fichier " << strFileName << "...";
  ifstream filebin{ strFileName + ".bin", ios_base::in | ios_base::binary };
  if(filebin.is_open())
  {
    cout << endl << "Chargement depuis la version binaire du fichier " << strFileName << "...";
    loadFromBinFile(filebin);
    cout << "Ok." << endl;
    return true;
  }

  ifstream file{ strFileName };
  if(!file.is_open())
    return false;

  //Trouve tous les éléments <vertexbuffer> pour spécifier le VBO à la taille max
  while (!file.eof())
  {
    string element{ xml_getNextElement(file) };
    if (regex_match(element, regex{ "[\\ \\t\\r\\n]*<vertexbuffer .*>" }))
    {
      bool bVal;
      string str;
      //positions
      str = xml_getElementAttribute(element, "positions");
      bVal = (str.compare("true") == 0);
      if (bVal)
        m_nNbPositionComponents = max(m_nNbPositionComponents, GLuint(3));
      //normales
      str = xml_getElementAttribute(element, "normals");
      bVal = (str.compare("true") == 0);
      if (bVal)
        m_nNbNormalComponents = max(m_nNbNormalComponents, GLuint(3));
      //textures coords (unité 0)
      str = xml_getElementAttribute(element, "texture_coord_dimensions_0");
      smatch substr_match;
      if (regex_search(str, substr_match, regex{ "[0-9]+" }))
      {
        str = substr_match[0].str();
        m_nNbTexCoordComponents = max(m_nNbTexCoordComponents, (GLuint)stoi(str));
      }
    }
  }
  m_nPositionOffset = 0;
  m_nNormalOffset = m_nNbPositionComponents;
  m_nTexCoordOffset = m_nNormalOffset + m_nNbNormalComponents;
  file.seekg(0);

  while(!file.eof())
  {
    string element{ xml_getNextElement(file) };
    
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*<submesh .*>" }))
      xmlSubmeshElement(element, file);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*<sharedgeometry .*>" }))
      xmlSharedgeometryElement(element, file);
  }

  cout << endl << "Enregistrement d'une version binaire du fichier " << strFileName << "...";
  saveToBinFile(strFileName + ".bin");

  cout << "Ok." << endl;
  return true;
}

void CMeshLoaderXML::loadFromBinFile(std::ifstream& file)
{
  size_t nBufSize;
  //VBO
  file.read(reinterpret_cast<char*>(&nBufSize), sizeof(nBufSize));
  m_vVBO.resize(nBufSize);
  for(GLfloat& fVal : m_vVBO)
    file.read(reinterpret_cast<char*>(&fVal), sizeof(fVal));

  //EBO
  file.read(reinterpret_cast<char*>(&nBufSize), sizeof(nBufSize));
  m_vEBO.resize(nBufSize);
  for(GLuint& nVal : m_vEBO)
    file.read(reinterpret_cast<char*>(&nVal), sizeof(nVal));

  //données internes
  file.read(reinterpret_cast<char*>(&m_nNbPositionComponents), sizeof(m_nNbPositionComponents));
  file.read(reinterpret_cast<char*>(&m_nNbNormalComponents), sizeof(m_nNbNormalComponents));
  file.read(reinterpret_cast<char*>(&m_nNbColorComponents), sizeof(m_nNbColorComponents));
  file.read(reinterpret_cast<char*>(&m_nNbTexCoordComponents), sizeof(m_nNbTexCoordComponents));
  file.read(reinterpret_cast<char*>(&m_nPositionOffset), sizeof(m_nPositionOffset));
  file.read(reinterpret_cast<char*>(&m_nNormalOffset), sizeof(m_nNormalOffset));
  file.read(reinterpret_cast<char*>(&m_nColorOffset), sizeof(m_nColorOffset));
  file.read(reinterpret_cast<char*>(&m_nTexCoordOffset), sizeof(m_nTexCoordOffset));
  file.read(reinterpret_cast<char*>(&m_ePrimitives), sizeof(m_ePrimitives));

  //Bounding box
  for(int i = 0; i < 3; ++i)
  {
    file.read(reinterpret_cast<char*>(&m_BoundingBox.m_fmin[i]), sizeof(m_BoundingBox.m_fmin[i]));
    file.read(reinterpret_cast<char*>(&m_BoundingBox.m_fmax[i]), sizeof(m_BoundingBox.m_fmax[i]));
  }

  m_subMeshes.loadFromBinFile(file);
}

void CMeshLoaderXML::saveToBinFile(const std::string& strFileName) const
{
  ofstream file{ strFileName, ios_base::out | ios_base::binary };

  size_t nBuffSize;
  //VBO
  nBuffSize = m_vVBO.size();
  file.write(reinterpret_cast<const char*>(&nBuffSize), sizeof(nBuffSize));
  for(const GLfloat& fVal : m_vVBO)
    file.write(reinterpret_cast<const char*>(&fVal),sizeof(fVal));

  //EBO
  nBuffSize = m_vEBO.size();
  file.write(reinterpret_cast<const char*>(&nBuffSize), sizeof(nBuffSize));
  for(const GLuint& nVal : m_vEBO)
    file.write(reinterpret_cast<const char*>(&nVal), sizeof(nVal));

  //données internes
  file.write(reinterpret_cast<const char*>(&m_nNbPositionComponents), sizeof(m_nNbPositionComponents));
  file.write(reinterpret_cast<const char*>(&m_nNbNormalComponents), sizeof(m_nNbNormalComponents));
  file.write(reinterpret_cast<const char*>(&m_nNbColorComponents), sizeof(m_nNbColorComponents));
  file.write(reinterpret_cast<const char*>(&m_nNbTexCoordComponents), sizeof(m_nNbTexCoordComponents));
  file.write(reinterpret_cast<const char*>(&m_nPositionOffset), sizeof(m_nPositionOffset));
  file.write(reinterpret_cast<const char*>(&m_nNormalOffset), sizeof(m_nNormalOffset));
  file.write(reinterpret_cast<const char*>(&m_nColorOffset), sizeof(m_nColorOffset));
  file.write(reinterpret_cast<const char*>(&m_nTexCoordOffset), sizeof(m_nTexCoordOffset));
  file.write(reinterpret_cast<const char*>(&m_ePrimitives), sizeof(m_ePrimitives));

  //Bounding box
  for(int i = 0; i < 3; ++i)
  {
    file.write(reinterpret_cast<const char*>(&m_BoundingBox.m_fmin[i]), sizeof(m_BoundingBox.m_fmin[i]));
    file.write(reinterpret_cast<const char*>(&m_BoundingBox.m_fmax[i]), sizeof(m_BoundingBox.m_fmax[i]));
  }

  m_subMeshes.saveToBinFile(file);
}

std::string CMeshLoaderXML::xml_getNextElement(std::ifstream& file)
{
  string element;
  getline(file, element, '>');
  return element + '>';
}

std::string CMeshLoaderXML::xml_getElementAttribute(std::string& element, const std::string& strAttributeName)
{
  smatch substr_match;
  if(!regex_search(element, substr_match, regex{ strAttributeName + "[ \\t]*=[ \\t]*\"[^\"]*\"" }))
    return string{};
  string str = substr_match[0].str();
  str = str.substr(str.find('\"') + 1);
  return str.substr(0, str.length() - 1);
}

bool CMeshLoaderXML::xml_isElementAttribute(std::string& element, const std::string& strAttributeName)
{
  return false;
}

void CMeshLoaderXML::xmlSharedgeometryElement(std::string& element, std::ifstream& file)
{
  int nNbVertex;
  string str{ xml_getElementAttribute(element, "vertexcount") };
  nNbVertex = stoi(str);

  GLuint iStartVertex = static_cast<GLuint>(m_vVBO.size()) / getStride();
  m_vVBO.resize(m_vVBO.size() + nNbVertex*getStride());

  while (!file.eof())
  {
    element = xml_getNextElement(file);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*<vertexbuffer .*>" }))
      xmlVertexbufferElement(element, file, iStartVertex);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*</sharedgeometry>" }))
      return;
  }
}

void CMeshLoaderXML::xmlSubmeshElement(std::string& element, std::ifstream& file)
{
  string strTextureFile{ xml_getElementAttribute(element, "material") };
  strTextureFile.append(".png");
  bool bUsedSharedVertices = (xml_getElementAttribute(element, "usesharedvertices").compare("true") == 0);
  if(xml_getElementAttribute(element, "operationtype").compare("triangle_list") != 0)
  {
    string str{ "Le mode de traçage " + xml_getElementAttribute(element, "operationtype") + " n'est pas pris en charge" };
    cerr << str << endl;
    throw runtime_error{ str };
  }
  else
    m_ePrimitives = GL_TRIANGLES;

  while(!file.eof())
  {
    element = xml_getNextElement(file);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*<faces .*>" }))
      xmlFacesElement(element, file, strTextureFile, bUsedSharedVertices);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*<geometry .*>" }))
      xmlGeometryElement(element, file);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*</submesh>" }))
      return;
  }
}

void CMeshLoaderXML::xmlFacesElement(std::string& element, std::ifstream& file, const std::string& strTextureFile, bool bUsedSharedVertices)
{
  string str{ xml_getElementAttribute(element, "count") };
  int nNbFaces;
  nNbFaces = stoi(str);
  m_vEBO.reserve(m_vEBO.size() + nNbFaces * 3);
  m_subMeshes.addMesh(nNbFaces * 3, strTextureFile);
  GLuint nStartIndice = 0;
  if(!bUsedSharedVertices && m_vVBO.size() != 0)
    nStartIndice = static_cast<GLuint>(m_vVBO.size())/getStride();

  while(!file.eof())
  {
    element = xml_getNextElement(file);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*<face .*>" }))
      xmlFaceElement(element, file, nStartIndice);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*</faces>" }))
      return;
  }
}

void CMeshLoaderXML::xmlFaceElement(std::string& element, std::ifstream& file, GLuint nStartIndice)
{
  string strAttrs[] = { "v1", "v2", "v3" };
  string str;
  GLuint i;
  for(auto strAttr : strAttrs)
  {
    str = xml_getElementAttribute(element, strAttr);
    i = stoi(str);
    i += nStartIndice;
    m_vEBO.push_back(i);
  }
}

void CMeshLoaderXML::xmlGeometryElement(std::string& element, std::ifstream& file)
{
  string str{ xml_getElementAttribute(element, "vertexcount") };
  GLuint nNbVertex;
  nNbVertex = stoi(str);

  GLuint iStartVertex = static_cast<GLuint>(m_vVBO.size()) / getStride();
  m_vVBO.resize(m_vVBO.size() + nNbVertex*getStride());

  while(!file.eof())
  {
    element = xml_getNextElement(file);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*<vertexbuffer .*>" }))
      xmlVertexbufferElement(element, file, iStartVertex);
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*</geometry>" }))
      return;
  }
}

void CMeshLoaderXML::xmlVertexbufferElement(std::string& element, std::ifstream& file, GLuint iStartVertex)
{
  GLuint iVertex = iStartVertex;
  while(!file.eof())
  {
    element = xml_getNextElement(file);
    if (regex_match(element, regex{ "[\\ \\t\\r\\n]*<vertex>" }))
    {
      xmlVertexElement(element, file, iVertex);
      iVertex++;
    }
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*</vertexbuffer>" }))
      return;
  }
}

void CMeshLoaderXML::xmlVertexElement(std::string& element, std::ifstream& file, GLuint iVertex)
{
  vector<GLfloat> vertex;
  vertex.resize(getStride());
  bool bPosition = false;
  bool bNormal = false;
  bool bTexCoords = false;

  while (!file.eof())
  {
    element = xml_getNextElement(file);
    if (regex_match(element, regex{ "[\\ \\t\\r\\n]*<position .*>" }))
    {
      xmlPositionElement(element, vertex);
      bPosition = true;
    }
    if (regex_match(element, regex{ "[\\ \\t\\r\\n]*<normal .*>" }))
    {
      xmlNormalElement(element, vertex);
      bNormal = true;
    }
    if (regex_match(element, regex{ "[\\ \\t\\r\\n]*<texcoord .*>" }))
    {
      xmlTexcoordElement(element, vertex);
      bTexCoords = true;
    }
    if(regex_match(element, regex{ "[\\ \\t\\r\\n]*</vertex>" }))
    {
      if (bPosition)
      {
        for (size_t i = 0; i < m_nNbPositionComponents; ++i)
          m_vVBO[iVertex*getStride() + m_nPositionOffset + i] = vertex[m_nPositionOffset + i];
        if (iVertex == 0)
          for (int i = 0; i < 3; ++i)
          {
            m_BoundingBox.m_fmin[i] = vertex[m_nPositionOffset + i];
            m_BoundingBox.m_fmax[i] = vertex[m_nPositionOffset + i];
          }
      }
      if (bNormal)
        for (size_t i = 0; i < m_nNbNormalComponents; ++i)
          m_vVBO[iVertex*getStride() + m_nNormalOffset + i] = vertex[m_nNormalOffset + i];
      if (bTexCoords)
        for (size_t i = 0; i < m_nNbTexCoordComponents; ++i)
          m_vVBO[iVertex*getStride() + m_nTexCoordOffset + i] = vertex[m_nTexCoordOffset + i];

      return;
    }
  }
}

void CMeshLoaderXML::xmlPositionElement(std::string& element, std::vector<GLfloat>& vertex)
{
  GLfloat x;
  GLfloat y;
  GLfloat z;
  string str;
  str = xml_getElementAttribute(element, "x");
  x = stof(str);
  str = xml_getElementAttribute(element, "y");
  y = stof(str);
  str = xml_getElementAttribute(element, "z");
  z = stof(str);
  vertex[m_nPositionOffset + 0] = x;
  vertex[m_nPositionOffset + 1] = y;
  vertex[m_nPositionOffset + 2] = z;

  for(int i = 0; i < 3; ++i)
  {
    if(m_BoundingBox.m_fmin[i] > vertex[m_nPositionOffset + i])
      m_BoundingBox.m_fmin[i] = vertex[m_nPositionOffset + i];
    if(m_BoundingBox.m_fmax[i] < vertex[m_nPositionOffset + i])
      m_BoundingBox.m_fmax[i] = vertex[m_nPositionOffset + i];
  }

}

void CMeshLoaderXML::xmlNormalElement(std::string& element, std::vector<GLfloat>& vertex)
{
  GLfloat x;
  GLfloat y;
  GLfloat z;
  string str;
  str = xml_getElementAttribute(element, "x");
  x = stof(str);
  str = xml_getElementAttribute(element, "y");
  y = stof(str);
  str = xml_getElementAttribute(element, "z");
  z = stof(str);

  vertex[m_nNormalOffset + 0] = x;
  vertex[m_nNormalOffset + 1] = y;
  vertex[m_nNormalOffset + 2] = z;
}

void CMeshLoaderXML::xmlTexcoordElement(std::string& element, std::vector<GLfloat>& vertex)
{
  string texCoords[] = {"u", "v", "w", "x"};
  string str;
  GLfloat fVal;
  for(GLuint i = 0; i < m_nNbTexCoordComponents; ++i)
  {
    str = xml_getElementAttribute(element, texCoords[i]);
    fVal = stof(str);
    vertex[m_nTexCoordOffset + i] = fVal;
  }
}

void CMeshLoaderXML::getBoundingBox(GLfloat& fXmin, GLfloat& fXmax, GLfloat& fYmin, GLfloat& fYmax, GLfloat& fZmin, GLfloat& fZmax) const
{
  fXmin = m_BoundingBox.m_fmin[0];
  fYmin = m_BoundingBox.m_fmin[1];
  fZmin = m_BoundingBox.m_fmin[2];
  fXmax = m_BoundingBox.m_fmax[0];
  fYmax = m_BoundingBox.m_fmax[1];
  fZmax = m_BoundingBox.m_fmax[2];
}
