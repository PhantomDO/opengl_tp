#version 330 core

out vec4 color;

struct sLight
{
	vec3 position;
	vec3 ambiant;
	vec3 diffus;
	vec3 specular;
};

uniform sLight light;

void main()
{
	color = vec4(light.diffus, 1.0);
}
