#pragma once
#include <GL/glew.h>
#include <vector>
#include <string>

class CMeshLoaderXML
{
public:
  CMeshLoaderXML();
private:
  //données
  std::vector<GLfloat> m_vVBO;
  std::vector<GLuint> m_vEBO;

  GLuint m_nNbPositionComponents;
  GLuint m_nNbNormalComponents;
  GLuint m_nNbColorComponents;
  GLuint m_nNbTexCoordComponents;
  GLuint m_nPositionOffset;
  GLuint m_nNormalOffset;
  GLuint m_nColorOffset;
  GLuint m_nTexCoordOffset;
  GLenum m_ePrimitives;

  struct BoundingBox
  {
    GLfloat m_fmin[3];
    GLfloat m_fmax[3];
  } m_BoundingBox;

  class CSubMeshes
  {
  private:
    std::vector<GLsizei> m_vMeshesNbIndices;
    std::vector<GLvoid *> m_vMeshesStartByteOffsets;
    std::vector<std::string> m_vTextureFiles;
    bool m_bSameTexture = true;
  public:
    CSubMeshes() {}
    CSubMeshes(CSubMeshes&& submeshes);
    void addMesh(GLsizei nNbIndices, const std::string& strTextureFile);
    GLsizei getNbMeshes() const { return static_cast<GLsizei>(m_vMeshesNbIndices.size()); }
    const GLsizei * getMeshesNbIndices() const { return m_vMeshesNbIndices.data(); }
    const GLvoid* const * getMeshesStartByteOffsets() const { return m_vMeshesStartByteOffsets.data(); }
    const std::string& getTextureFile(size_t i) const { return m_vTextureFiles[i]; }
    bool isSameTexture() const { return m_bSameTexture; }
    void loadFromBinFile(std::ifstream& file);
    void saveToBinFile(std::ofstream& file) const;
    void clear();
  } m_subMeshes;

  std::string xml_getNextElement(std::ifstream& file);
  std::string xml_getElementAttribute(std::string& element, const std::string& strAttributeName);
  bool xml_isElementAttribute(std::string& element, const std::string& strAttributeName);
  void xmlSharedgeometryElement(std::string& element, std::ifstream& file);
  void xmlSubmeshElement(std::string& element, std::ifstream& file);
  void xmlFacesElement(std::string& element, std::ifstream& file, const std::string& strTextureFile, bool bUsedSharedVertices);
  void xmlFaceElement(std::string& element, std::ifstream& file, GLuint nStartIndice);
  void xmlGeometryElement(std::string& element, std::ifstream& file);
  void xmlVertexbufferElement(std::string& element, std::ifstream& file, GLuint iStartVertex);
  void xmlVertexElement(std::string& element, std::ifstream& file, GLuint iVertex);
  void xmlPositionElement(std::string& element, std::vector<GLfloat>& vertex);
  void xmlNormalElement(std::string& element, std::vector<GLfloat>& vertex);
  void xmlTexcoordElement(std::string& element, std::vector<GLfloat>& vertex);

  void loadFromBinFile(std::ifstream& file);
  void saveToBinFile(const std::string& strFileName) const;

public:

  CMeshLoaderXML(CMeshLoaderXML&& mesh);

  bool loadFromFile(const std::string& strFileName);
  const GLfloat* getVBO() const { return m_vVBO.data(); }
  const GLuint* getEBO() const { return m_vEBO.data(); }
  size_t getVBOSize() const { return m_vVBO.size(); }
  size_t getEBOSize() const { return m_vEBO.size(); }
  GLsizei getNbMeshes() const { return m_subMeshes.getNbMeshes(); }
  const GLsizei * getMeshesNbIndices() const { return m_subMeshes.getMeshesNbIndices(); }
  const GLvoid* const * getMeshesStartByteOffsets() const { return m_subMeshes.getMeshesStartByteOffsets(); }
  bool isAllMeshesSameTexture() const { return m_subMeshes.isSameTexture(); }
  const std::string& getMeshTextureFile(size_t i) const { return m_subMeshes.getTextureFile(i); }

public:
  GLuint getNbPositionComponents() const { return m_nNbPositionComponents; }
  GLuint getNbNormalComponents() const { return m_nNbNormalComponents; }
  GLuint getNbColorComponents() const { return m_nNbColorComponents; }
  GLuint getNbTexCoordComponents() const { return m_nNbTexCoordComponents; }
  GLuint getPositionOffset() const { return m_nPositionOffset; }
  GLuint getNormalOffset() const { return m_nNormalOffset; }
  GLuint getColorOffset() const { return m_nColorOffset; }
  GLuint getTexCoordOffset() const { return m_nTexCoordOffset; }
  GLenum getPrimitivesType() const { return m_ePrimitives; }
  GLuint getStride() const { return m_nNbPositionComponents + m_nNbNormalComponents + m_nNbColorComponents + m_nNbTexCoordComponents; }
  void getBoundingBox(GLfloat& fXmin, GLfloat& fXmax, GLfloat& fYmin, GLfloat& fYmax, GLfloat& fZmin, GLfloat& fZmax) const;
  GLfloat getXmin() const { return m_BoundingBox.m_fmin[0]; }
  GLfloat getYmin() const { return m_BoundingBox.m_fmin[1]; }
  GLfloat getZmin() const { return m_BoundingBox.m_fmin[2]; }
  GLfloat getXmax() const { return m_BoundingBox.m_fmax[0]; }
  GLfloat getYmax() const { return m_BoundingBox.m_fmax[1]; }
  GLfloat getZmax() const { return m_BoundingBox.m_fmax[2]; }
};

