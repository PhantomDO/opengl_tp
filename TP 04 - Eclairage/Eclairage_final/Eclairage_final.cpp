// OpenGLStats.cpp : définit le point d'entrée pour l'application console.
//

#include "stdafx.h"
#include "helperFunc.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ShaderProgram.h>
#include "MeshLoaderXML.h"
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <FreeImagePlus.h>
#include <Texture.h>
#include <memory>

using namespace std;

#ifdef WIN32
//This magic line is to force notebook computer that share NVidia and Intel graphics to use high performance GPU (NVidia).
//Intel graphics doesn't support OpenGL > 1.2
extern "C" _declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
#endif

const GLfloat pi = 3.1415926535897932384626433832795f;


void sizefunct(GLFWwindow* pWnd, int width, int height);
void keyfunct(GLFWwindow* pWnd, int key, int scancode, int action, int mods);
void scrollfunct(GLFWwindow* pWnd, double xoffset, double yoffset);


struct WndData
{
  float m_fFovy = 45.0f;
  GLuint programId;
  GLuint lightProgramId;
};

int main(void)
{
#ifdef WIN32
  //Passe la console en UTF-8, permettant d'afficher les accents. Tous les fichiers de cette solution
  //sont encodés en UTF-8.
  SetConsoleOutputCP(65001);
#endif
  //#############################################################################
  //                    Initialisation de la fenêtre
  //#############################################################################
  const int nInitWndWidth = 800;
  const int nInitWndHeight = 600;

  if (!glfwInit())
    exit(EXIT_FAILURE);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_SAMPLES, 16); //Multisample
  glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
  GLFWwindow* pWnd = glfwCreateWindow(nInitWndWidth, nInitWndHeight, "Hello World", nullptr, nullptr);
  if (!pWnd)
    terminate("Impossible de créer la fenêtre !");

  glfwMakeContextCurrent(pWnd);
  glEnable(GL_MULTISAMPLE);
  //glDisable(GL_MULTISAMPLE);

  glewExperimental = GL_TRUE;
  GLenum err;
  if((err = glewInit()) != GLEW_OK) /* Problem: glewInit failed, something is seriously wrong. */
    terminate(string("Error: ") + reinterpret_cast<const char*>(glewGetErrorString(err)));

  PrintRendererInfo();

  WndData wndData;
  glfwSetWindowUserPointer(pWnd, &wndData);

  glfwSetKeyCallback(pWnd, keyfunct);
  glfwSetScrollCallback(pWnd, scrollfunct);
  glfwSetWindowSizeCallback(pWnd, sizefunct);

  //#############################################################################
  //                    Sélection de l'objet central
  //#############################################################################
  string strMeshFileNames[] = {
    "cube.mesh.xml",
    "knot.mesh.xml",
    "ogrehead.mesh.xml",
    "autre fichier..."
  };
  string strMeshFileName;
  cout << "Choisissez un fichier mesh à charger : " << endl;
  for(int i = 0; i < sizeof(strMeshFileNames) / sizeof(string); ++i)
    cout << " " << i + 1 << " : " << strMeshFileNames[i] << endl;
  cout << "Entrez une valeur : ";
  unsigned int nMeshFileName;
  cin >> nMeshFileName;
  if (nMeshFileName > 4) nMeshFileName = 4;
  if (nMeshFileName < 1) nMeshFileName = 1;
  if (nMeshFileName == 4)
  {
    cout << "Entrez un nom de fichier : ";
    cin.ignore();
    getline(cin, strMeshFileName);
  }
  else
    strMeshFileName = strMeshFileNames[nMeshFileName-1];

  GLuint nNbUsedLights;
  cout << "Choisissez le nombre de lumière (1 ou 2) : ";
  cin >> nNbUsedLights;
  if (nNbUsedLights > 2) nNbUsedLights = 2;
  if (nNbUsedLights < 1) nNbUsedLights = 1;
  //#############################################################################
  //                    Chargement des meshes
  //#############################################################################
  CMeshLoaderXML mesh;
  if (!mesh.loadFromFile(strMeshFileName))
    terminate("Impossible de charger le fichier mesh.xml");

  //Affichage de la fenêtre
  glfwShowWindow(pWnd);

  GLfloat lightCubeVertices[] = {
    //Face avant    
    -0.5f, -0.5f,  0.5f,    //inf gauche rouge
     0.5f, -0.5f,  0.5f,    //inf droit  vert
     0.5f,  0.5f,  0.5f,    //sup droit  jaune
    -0.5f,  0.5f,  0.5f,    //sup gauche bleu
    //Face arrière    
    -0.5f, -0.5f, -0.5f,    //inf gauche magenta
     0.5f, -0.5f, -0.5f,    //inf droit  cyan
     0.5f,  0.5f, -0.5f,    //sup droit  blanc 
    -0.5f,  0.5f, -0.5f     //sup gauche gris
  };

  GLuint lightCubeIndices[] = {
    //Face avant    
    0, 1, 3, 3, 1, 2,
    //Face droite
    2, 1, 5, 2, 5, 6,
    //Face arrière
    6, 5, 4, 6, 4, 7,
    //Face gauche
    7, 4, 0, 7, 0, 3,
    //Face haut
    3, 2, 7, 7, 2, 6,
    //Face bas
    4, 5, 0, 0, 5, 1
  };


  //#############################################################################
  //                    Compilation des shaders
  //#############################################################################
  CShaderProgram program{ CShader{ GL_VERTEX_SHADER, GetShaderSourceFromRessource(_T("vertex.vert")) },
                          CShader{ GL_FRAGMENT_SHADER, GetShaderSourceFromRessource(_T("fragment.frag")) } };

  wndData.programId = program.GetProgramId();

  CShaderProgram lightProgram{ CShader{ GL_VERTEX_SHADER, GetShaderSourceFromRessource(_T("lightVertex.vert")) },
                               CShader{ GL_FRAGMENT_SHADER, GetShaderSourceFromRessource(_T("lightFragment.frag")) } };

  wndData.lightProgramId = lightProgram.GetProgramId();




  //#############################################################################
  //                    Chargement des textures
  //#############################################################################
  vector<std::unique_ptr<CTexture>> vpTex;
  if(mesh.isAllMeshesSameTexture())
  {
    vpTex.push_back(std::make_unique<CTexture>());
    //!\todo Charger les textures dans les bonnes unités (par ex. texture diffuse : unité 0, texture spéculaire : unité 1)
    if (!vpTex.back()->loadTexture(mesh.getMeshTextureFile(0)))
      vpTex.back()->loadTexture("BaseWhite.png");
    vpTex.back()->setParam(GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    vpTex.back()->setParam(GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    vpTex.back()->setParam(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    vpTex.back()->setParam(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }
  else
  {
    vpTex.reserve(mesh.getNbMeshes());
    for(GLsizei i = 0; i < mesh.getNbMeshes(); ++i)
    {
      vpTex.push_back(std::make_unique<CTexture>());
      //!\todo Charger les textures dans les bonnes unités (par ex. texture diffuse : unité 0, texture spéculaire : unité 1)
      if (!vpTex.back()->loadTexture(mesh.getMeshTextureFile(i)))
        vpTex.back()->loadTexture("BaseWhite.png");
      vpTex.back()->setParam(GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
      vpTex.back()->setParam(GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
      vpTex.back()->setParam(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      vpTex.back()->setParam(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
  }







  //#############################################################################
  //      Initialisation des matrices de modélisation et de visualisation 
  //                           et de projection
  //#############################################################################
  glm::vec3 meshPmin{ mesh.getXmin(), mesh.getYmin(), mesh.getZmin() };
  glm::vec3 meshPmax{ mesh.getXmax(), mesh.getYmax(), mesh.getZmax() };
  glm::vec3 meshCenter((meshPmax + meshPmin) / 2.0f);
  GLfloat meshSize = glm::distance(meshPmin, meshPmax);

  glm::mat4 meshModelMatrix{ 1 };
  glm::mat4 lightModelMatrix = glm::scale(glm::vec3(meshSize) / 40.0f);
  glm::mat4 ViewMatrix = glm::lookAt(meshCenter + glm::vec3{ 0.0f, 0.0f, 2 * meshSize }, meshCenter, glm::vec3{ 0.0f, 1.0f, 0.0f });
  sizefunct(pWnd, nInitWndWidth, nInitWndHeight);
  //récupère les positions des uniforms correspondant à ces matrices
  GLint nUniform_meshModelMatrix = glGetUniformLocation(program.GetProgramId(), "model");
  GLint nUniform_meshViewMatrix = glGetUniformLocation(program.GetProgramId(), "view");
  GLint nUniform_lightModelMatrix = glGetUniformLocation(lightProgram.GetProgramId(), "model");
  GLint nUniform_lightViewMatrix = glGetUniformLocation(lightProgram.GetProgramId(), "view");
  //transfère la matrice de visualisation (ne changera plus, la caméra est fixe)
  program.Use();
  glUniformMatrix4fv(nUniform_meshViewMatrix, 1, GL_FALSE, glm::value_ptr(ViewMatrix));
  lightProgram.Use();
  glUniformMatrix4fv(nUniform_lightViewMatrix, 1, GL_FALSE, glm::value_ptr(ViewMatrix));
  glUniformMatrix4fv(nUniform_lightModelMatrix, 1, GL_FALSE, glm::value_ptr(lightModelMatrix));


  //#############################################################################
  //      Transfert des meshes vers le GPU et spécification des vertex
  //#############################################################################
  //Allocation des buffers
  GLuint meshVAO;
  glGenVertexArrays(1, &meshVAO);
  GLuint meshVBO;
  glGenBuffers(1, &meshVBO);

  //Spécification des vertices pour le mesh
  // Le meshVAO
  glBindVertexArray(meshVAO);
  // Le meshVBO
  glBindBuffer(GL_ARRAY_BUFFER, meshVBO);
  glBufferData(GL_ARRAY_BUFFER, mesh.getVBOSize() * sizeof(GLfloat), mesh.getVBO(), GL_STATIC_DRAW);

  //spécifie l'organisation de l'entrée du vertex shader
  //Positions
  glVertexAttribPointer(0, mesh.getNbPositionComponents(), GL_FLOAT, GL_FALSE, mesh.getStride() * sizeof(GLfloat), reinterpret_cast<GLvoid*>(mesh.getPositionOffset() * sizeof(GLfloat)));
  glEnableVertexAttribArray(0);
  //normales
  glVertexAttribPointer(1, mesh.getNbNormalComponents(), GL_FLOAT, GL_FALSE, mesh.getStride() * sizeof(GLfloat), reinterpret_cast<GLvoid*>(mesh.getNormalOffset() * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);
  //Coordonnées de textures
  glVertexAttribPointer(2, mesh.getNbTexCoordComponents(), GL_FLOAT, GL_FALSE, mesh.getStride() * sizeof(GLfloat), reinterpret_cast<GLvoid*>(mesh.getTexCoordOffset() * sizeof(GLfloat)));
  glEnableVertexAttribArray(2);

  //Le EBO
  GLuint EBO;
  glGenBuffers(1, &EBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.getEBOSize() * sizeof(GLuint), mesh.getEBO(), GL_STATIC_DRAW);


  //Spécification des vertices pour le lightcube
  GLuint lightCubeVAO;
  glGenVertexArrays(1, &lightCubeVAO);
  GLuint lightCubeVBO;
  glGenBuffers(1, &lightCubeVBO);
  // Le meshVAO
  glBindVertexArray(lightCubeVAO);
  // Le meshVBO
  glBindBuffer(GL_ARRAY_BUFFER, lightCubeVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(lightCubeVertices), lightCubeVertices, GL_STATIC_DRAW);

  //spécifie l'organisation de l'entrée du vertex shader
  //Positions
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<GLvoid*>(0));
  glEnableVertexAttribArray(0);

  //Le EBO
  GLuint lightCubeEBO;
  glGenBuffers(1, &lightCubeEBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, lightCubeEBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(lightCubeIndices), lightCubeIndices, GL_STATIC_DRAW);



  //#############################################################################
  //                          Définition de la lumière
  //#############################################################################

  struct sLight
  {
	  glm::vec3 position;
	  glm::vec3 ambiant;
	  glm::vec3 diffus;
	  glm::vec3 specular;
  } light[2];

  struct nParameter
  {
	  GLuint nLightPos;
	  GLuint nLightAmbiant;
	  GLuint nLightDiffus;
	  GLuint nLightSpec;
  } params[2], P[2];

  light[0].ambiant = glm::vec3{ 0.1f, 0.1f, 0.1f };
  light[0].diffus = glm::vec3{ 1.0f, 1.0f, 0.8f };
  light[0].specular = glm::vec3{ 1.0f, 1.0f, 0.8f };

  light[1].ambiant = glm::vec3{ 0.7f, 0.4f, 1.0f };
  light[1].diffus = glm::vec3{ 0.2f, 0.7f, 0.2f };
  light[1].specular = glm::vec3{ 1.0f, 1.0f, 0.8f };

  params[0].nLightPos = glGetUniformLocation(lightProgram.GetProgramId(), "light.position");
  params[0].nLightAmbiant = glGetUniformLocation(lightProgram.GetProgramId(), "light.ambiant");
  params[0].nLightDiffus = glGetUniformLocation(lightProgram.GetProgramId(), "light.diffus");
  params[0].nLightSpec = glGetUniformLocation(lightProgram.GetProgramId(), "light.specular");

  params[1].nLightPos = glGetUniformLocation(lightProgram.GetProgramId(), "light.position");
  params[1].nLightAmbiant = glGetUniformLocation(lightProgram.GetProgramId(), "light.ambiant");
  params[1].nLightDiffus = glGetUniformLocation(lightProgram.GetProgramId(), "light.diffus");
  params[1].nLightSpec = glGetUniformLocation(lightProgram.GetProgramId(), "light.specular");

  GLuint nbUsedLight = glGetUniformLocation(lightProgram.GetProgramId(), "nbUsedLight");

  P[0].nLightPos = glGetUniformLocation(program.GetProgramId(), "light[0].position");
  P[0].nLightAmbiant = glGetUniformLocation(program.GetProgramId(), "light[0].ambiant");
  P[0].nLightDiffus = glGetUniformLocation(program.GetProgramId(), "light[0].diffus");
  P[0].nLightSpec = glGetUniformLocation(program.GetProgramId(), "light[0].specular");

  P[1].nLightPos = glGetUniformLocation(program.GetProgramId(), "light[1].position");
  P[1].nLightAmbiant = glGetUniformLocation(program.GetProgramId(), "light[1].ambiant");
  P[1].nLightDiffus = glGetUniformLocation(program.GetProgramId(), "light[1].diffus");
  P[1].nLightSpec = glGetUniformLocation(program.GetProgramId(), "light[1].specular");

  GLuint nbUsedLightMesh = glGetUniformLocation(program.GetProgramId(), "nbUsedLight");
  //#############################################################################
  //                          Boucle d'affichage
  //#############################################################################
  glEnable(GL_DEPTH_TEST);

  glfwSetTime(0);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  while(!glfwWindowShouldClose(pWnd))
  {
    glfwPollEvents();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLfloat t = static_cast<GLfloat>(glfwGetTime());
    //!\todo Définition de la position de la lumière

	//Calcul de la position de la lumière (tourne autour du mesh)
	//lPos += meshCenter;

	light[0].position.x = light[1].position.z = cos(t * 2.0f) * meshSize / 2;
	light[0].position.y = light[1].position.x = cos(t / 2.0f) * meshSize / 2;
	light[0].position.z = light[1].position.y = sin(t * 2.0f) * meshSize / 2;

	glm::vec4	lightPos = ViewMatrix * glm::vec4(light[0].position, 1.0f);
	light[0].position = glm::vec3(lightPos) / lightPos.w;
				lightPos = ViewMatrix * glm::vec4(light[1].position, 1.0f);
	light[1].position = glm::vec3(lightPos) / lightPos.w;
	
    //Affichage du mesh
    program.Use();
    glm::mat4 rotation = glm::rotate(t, glm::vec3{ 0.0f, 1.0f, 0.0f });
	glUniformMatrix4fv(nUniform_meshModelMatrix, 1, GL_FALSE, glm::value_ptr(meshModelMatrix * rotation));
	
	//LIGHT 
	glUniform3fv(P[0].nLightPos, 1, glm::value_ptr(light[0].position));
	glUniform3fv(P[0].nLightAmbiant, 1, glm::value_ptr(light[0].ambiant));
	glUniform3fv(P[0].nLightDiffus, 1, glm::value_ptr(light[0].diffus));
	glUniform3fv(P[0].nLightSpec, 1, glm::value_ptr(light[0].specular));

	glUniform3fv(P[1].nLightPos, 1, glm::value_ptr(light[1].position));
	glUniform3fv(P[1].nLightAmbiant, 1, glm::value_ptr(light[1].ambiant));
	glUniform3fv(P[1].nLightDiffus, 1, glm::value_ptr(light[1].diffus));
	glUniform3fv(P[1].nLightSpec, 1, glm::value_ptr(light[1].specular));

	glUniform1ui(nbUsedLightMesh, nNbUsedLights);

	glBindVertexArray(meshVAO);
    if(mesh.isAllMeshesSameTexture())
    {
      vpTex.back()->bind();
      glMultiDrawElements(mesh.getPrimitivesType(), mesh.getMeshesNbIndices(), GL_UNSIGNED_INT, (const GLvoid**)mesh.getMeshesStartByteOffsets(), mesh.getNbMeshes());
    }
    else
    {
      for(GLsizei i = 0; i < mesh.getNbMeshes(); ++i)
      {
        vpTex[i]->bind();
        glDrawElements(mesh.getPrimitivesType(), mesh.getMeshesNbIndices()[i], GL_UNSIGNED_INT, mesh.getMeshesStartByteOffsets()[i]);
      }
    }

    //affichage des cubes de lumière
    lightProgram.Use();
    glBindVertexArray(lightCubeVAO);
	glUniform1ui(nbUsedLight, nNbUsedLights); 
    for(GLuint i = 0; i < nNbUsedLights; ++i)
    {
      //!\todo transmettre la position et la couleur de la lumière au shader d'affichage des cubes
		glUniform3fv(params[i].nLightPos, 1, glm::value_ptr(light[i].position));
		glUniform3fv(params[i].nLightDiffus, 1, glm::value_ptr(light[i].diffus));
		glDrawElements(GL_TRIANGLES, sizeof(lightCubeIndices) / sizeof(GLuint), GL_UNSIGNED_INT, 0);
    }
    glfwSwapBuffers(pWnd);
  }

  glfwTerminate();

  return 0;
}



void keyfunct(GLFWwindow* pWnd, int key, int scancode, int action, int mods)
{
  if(action == GLFW_RELEASE)
    return;

  WndData& TheWndData = *reinterpret_cast<WndData*>(glfwGetWindowUserPointer(pWnd));

  switch (key)
  {
  case GLFW_KEY_W:
  {
    GLint modes[2];
    glGetIntegerv(GL_POLYGON_MODE, modes);
    if (modes[0] == GL_LINE)
      modes[0] = GL_FILL;
    else
      modes[0] = GL_LINE;
    glPolygonMode(GL_FRONT_AND_BACK, modes[0]);
    break;
  }
  }
}

void scrollfunct(GLFWwindow* pWnd, double xoffset, double yoffset)
{
  WndData& TheWndData = *reinterpret_cast<WndData*>(glfwGetWindowUserPointer(pWnd));

  int width, height;
  glfwGetWindowSize(pWnd, &width, &height);
  TheWndData.m_fFovy += static_cast<float>(yoffset);
  if(TheWndData.m_fFovy < 1.0f)TheWndData.m_fFovy = 1.0f;
  if(TheWndData.m_fFovy > 180.0f)TheWndData.m_fFovy = 180.0f;
  glm::mat4 projection = glm::perspective(glm::radians(TheWndData.m_fFovy), static_cast<float>(width) / static_cast<float>(height), 1.0f, 1000.0f);

  GLint nUniform_meshProjectionMatrix = glGetUniformLocation(TheWndData.programId, "proj");
  glUseProgram(TheWndData.programId);
  glUniformMatrix4fv(nUniform_meshProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projection));
  GLint nUniform_lightProjectionMatrix = glGetUniformLocation(TheWndData.lightProgramId, "proj");
  glUseProgram(TheWndData.lightProgramId);
  glUniformMatrix4fv(nUniform_lightProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projection));
}

void sizefunct(GLFWwindow* pWnd, int width, int height)
{
  if(width*height == 0)
    return;
  WndData &TheWndData = *(reinterpret_cast<WndData*>(glfwGetWindowUserPointer(pWnd)));

  glViewport(0, 0, width, height);

  glm::mat4 projection = glm::perspective(glm::radians(TheWndData.m_fFovy), static_cast<float>(width) / static_cast<float>(height), 1.0f, 1000.0f);

  GLint nUniform_meshProjectionMatrix = glGetUniformLocation(TheWndData.programId, "proj");
  glUseProgram(TheWndData.programId);
  glUniformMatrix4fv(nUniform_meshProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projection));
  GLint nUniform_lightProjectionMatrix = glGetUniformLocation(TheWndData.lightProgramId, "proj");
  glUseProgram(TheWndData.lightProgramId);
  glUniformMatrix4fv(nUniform_lightProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projection));
}

