#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>

#include "Color.h"
#include <vector>

extern "C" _declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;

void CursorPosCallback(GLFWwindow* window, double xpos, double ypos);
void KeyDownCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void ShaderCompilationTest(GLint vertexShader);
void ProgramShaderLinkedTest(GLint program, GLint vertexShader, GLint fragmentShader);
std::string ReadShaderFile(const char* shader);

int main(int argc, char* argv[])
{
	glfwInit();
	if (!glfwInit()) exit(EXIT_FAILURE); //Fonction de la biblioth�que GLFW

	glGetString(GL_VERSION); //Fonction de la biblioth�que OpenGL du syst�me

	// Initialisation du fenetre opengl en version 3.
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Cr�e la fenetre et la rend contextuable pour opengl
	GLFWwindow* pWnd = glfwCreateWindow(800, 600, "Hello World", nullptr, nullptr);
	if (!pWnd)
	{
		std::cerr << "Impossible de cr�er la fenetre!" << std::endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(pWnd); 

	glewExperimental = GL_TRUE;
	glewInit(); //Fonction de la biblioth�que GLEW

	// COLOR
	CColor* backgrondColor = new CColor(0.33f, 1.0f, 1.0f);
	std::cout	<< backgrondColor->GetR() << ", "
				<< backgrondColor->GetG() << ", "
				<< backgrondColor->GetB() << std::endl;

	CColor* orangeColor = new CColor(0.39f, 1.0f, 1.0f);
	CColor* blueColor = new CColor(1.0f, 1.0f, 1.0f);
	
	glClearColor(
		backgrondColor->GetR(),
		backgrondColor->GetG(),
		backgrondColor->GetB(),
		1.0f
	);

	// VERTICES
	GLfloat vertices[]{
		-0.5f	,	0.25f,
		 0.5f	,	0.25f,
		 0.5f	,	-0.25f,
		 -0.5f	,	-0.25f,

		 -0.5f	,	-0.25f,
		 0.5f	,	-0.25f,
		 0.5f	,	-0.75f,
		 -0.5f	,	-0.75f,
	};

	// VERTICES
	GLuint indices[]{
		0, 2, 3,
		0, 1, 2,

		4, 6, 7,
		4, 5, 6,
	};

	// VAO
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// VBO
	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Attribution du tableau au GPU qui le transform en vec2
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// EBO
	GLuint EBO;
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


	// vertex reading stream	
	std::string vertexStr = ReadShaderFile("triangleVertex.glsl");
	std::cout << vertexStr << std::endl;

	// Creation du vertexShader
	GLint vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLchar* vertexShaderSource = (GLchar*)(vertexStr).c_str();
	glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);
	glCompileShader(vertexShader);

	// test de compilation du vertex shader
	ShaderCompilationTest(vertexShader);

	// fragment reading stream
	std::string fragmentStr = ReadShaderFile("triangleFragment.glsl");
	std::cout << fragmentStr << std::endl;

	// Creation du fragmentShader
	GLint fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	GLchar* fragmentShaderSource = (GLchar*)(fragmentStr).c_str();
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, nullptr);
	glCompileShader(fragmentShader);

	// test de compilation du fragment shader
	ShaderCompilationTest(fragmentShader);

	// Creation du program
	GLint shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	// test de link du program avec les shader
	ProgramShaderLinkedTest(shaderProgram, vertexShader, fragmentShader);

	//glDeleteShader(vertexShader);
	//glDeleteShader(fragmentShader);
	
	// pointeur sur les touche du claver
	glfwSetKeyCallback(pWnd, KeyDownCallback);
	// pointeur sur la couleur de la fenetre
	glfwSetWindowUserPointer(pWnd, backgrondColor);
	// pointeur sur la position de la souris
	glfwSetCursorPosCallback(pWnd, CursorPosCallback);

	// utilise le programe cr�er precedement
	glUseProgram(shaderProgram);

	while (!glfwWindowShouldClose(pWnd))
	{
		glfwPollEvents();

		// change la couleur de la fenetre
		glClearColor(backgrondColor->GetR(), backgrondColor->GetG(), backgrondColor->GetB(), 1.0f);
		// remet la couleur par default
		glClear(GL_COLOR_BUFFER_BIT);

		// affiche le triangle depuis le vbo
		//glDrawArrays(GL_TRIANGLES, 0, 2);

		// affiche le rect depuis le ebo
		glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(0));
		// affiche ls triangles rempli
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		// affiche le rect depuis le ebo
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0));
		// affiche ls triangles en fil de fer
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glfwSwapBuffers(pWnd);
	}

	glDeleteProgram(shaderProgram);
	glDisableVertexAttribArray(0);

	glfwTerminate();

	return 0;
}

void CursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	int windowSizeX, windowSizeY;
	CColor& color = *(reinterpret_cast<CColor*>(glfwGetWindowUserPointer(window)));

	//std::cout << xpos << ", " << ypos << std::endl;

	// r�cup�re taille de la fenetre
	glfwGetWindowSize(window, &windowSizeX, &windowSizeY);
	
	color.SetHSV(color.GetH(), static_cast<float>(xpos/windowSizeX), static_cast<float>(ypos/windowSizeY));
}

void KeyDownCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
		return;

	CColor& color = *(reinterpret_cast<CColor*>(glfwGetWindowUserPointer(window)));
	float hValue = color.GetH();

	switch (key)
	{
	case GLFW_KEY_DOWN:
		hValue -= 1.0f;
		if (hValue < 0.0f)
			hValue = 359.0f;
		color.SetHSV(hValue, color.GetS(), color.GetV());
		break;
	case GLFW_KEY_UP:
		hValue += 1.0f;
		if (hValue > 360.0f)
			hValue = 0.0f;
		color.SetHSV(hValue, color.GetS(), color.GetV());
		break;
	case GLFW_KEY_W:
		break;
	default:
		break;
	}
}

void ShaderCompilationTest(GLint vertexShader)
{
	GLint isCompiled;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);

		for (auto& i : infoLog)
			std::cout << i;

		return;
	}
}

void ProgramShaderLinkedTest(GLint program, GLint vertexShader, GLint fragmentShader)
{
	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

		// We don't need the program anymore.
//		glDeleteProgram(program);
		// Don't leak shaders either.
//		glDeleteShader(vertexShader);
//		glDeleteShader(fragmentShader);
		
		// Use the infoLog as you see fit.

		// In this simple program, we'll just leave
		return;
	}
}

std::string ReadShaderFile(const char* shader)
{
	// lis le fichier 
	std::ifstream file(shader);
	if (!file) return std::string();

	size_t size = file.gcount();

	// remet les caract�re au d�part du fichier
	file.clear();
	file.seekg(0, std::ios_base::beg);

	// copie les caract�re dans le string stream
	std::stringstream sstr;
	sstr << file.rdbuf();
	file.close();

	// renvoie une string du string stream pass�
	return sstr.str();
}
