// OpenGLStats.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <FreeImagePlus.h>
#include "helperFunc.h"
#include <vector>
#include "CPolarMesh.h"

#ifdef WIN32
//This magic line is to force notebook computer that share NVidia and Intel graphics to use high performance GPU (NVidia).
//Intel graphics doesn't support OpenGL > 1.2
extern "C" _declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
#endif

constexpr GLfloat pi = 3.1415926535897932384626433832795f;


void characterfunct(GLFWwindow* pWnd, unsigned int codepoint);
void ScrollWheelCallback(GLFWwindow* pWnd, double xOffset, double yOffset);
void sizefunct(GLFWwindow* pWnd, int width, int height);

class WndData
{
public:
  float m_fFovy = 45.0f;
  GLuint programId;
  GLuint VAO;
  GLuint VBO;
  GLuint EBO;
  CPolarMesh polarMesh;
  GLint unifZScale;
  GLint unifNbRipples;
  GLint unifFoam;

  glm::mat4 ProjMatrix;
  GLint nProjectionMatrix;

  GLint WndHeight;
  GLint WndWidth;
};

int main(void)
{
  //#############################################################################
  //                    Initialisation de la fen�tre
  //#############################################################################
	constexpr int nInitWndWidth = 800;
	constexpr int nInitWndHeight = 800;

  if(!glfwInit())
    exit(EXIT_FAILURE);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_SAMPLES, 16); //Multisample
  glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
  GLFWwindow* pWnd = glfwCreateWindow(nInitWndWidth, nInitWndHeight, "Hello World", nullptr, nullptr);
  if(!pWnd)
    terminate("Impossible de cr�er la fen�tre !");

  glfwMakeContextCurrent(pWnd);
  glEnable(GL_MULTISAMPLE);

  glewExperimental = GL_TRUE;
  GLenum err;
  if((err = glewInit()) != GLEW_OK) /* Problem: glewInit failed, something is seriously wrong. */
    terminate(std::string("Error: ") + (const char*)glewGetErrorString(err));

  PrintRendererInfo();

  WndData wndData;

  wndData.WndHeight = nInitWndHeight;
  wndData.WndWidth = nInitWndWidth;

  glfwSetWindowUserPointer(pWnd, &wndData);

  glfwSetCharCallback(pWnd, characterfunct);
  glfwSetScrollCallback(pWnd, ScrollWheelCallback);
  glfwSetWindowSizeCallback(pWnd, sizefunct);

  //Affichage de la fen�tre
  glfwShowWindow(pWnd);

  //#############################################################################
  //                    Cr�ation de l'objet
  //#############################################################################

  //Allocation des buffers
  glGenVertexArrays(1, &wndData.VAO);
  glGenBuffers(1, &wndData.VBO);

  //Sp�cification des vertices pour le mesh
  // Le VAO
  glBindVertexArray(wndData.VAO);
  // Le VBO
  glBindBuffer(GL_ARRAY_BUFFER, wndData.VBO);
  glBufferData(GL_ARRAY_BUFFER, wndData.polarMesh.GetPolarVerticesBufferByteSize(), wndData.polarMesh.GetPolarVertices(), GL_STREAM_DRAW);

  //sp�cifie l'organisation de l'entr�e du vertex shader
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(0));
  glEnableVertexAttribArray(0);

  //Le EBO
  glGenBuffers(1, &wndData.EBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, wndData.EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, wndData.polarMesh.GetIndicesBufferByteSize(), wndData.polarMesh.GetIndices(), GL_STREAM_DRAW);


  //#############################################################################
  //                    Compilation des shaders
  //#############################################################################

  GLuint nShader = glCreateProgram();
  GLuint nVertexShader = glCreateShader(GL_VERTEX_SHADER);
  std::string strVertexShaderSource = GetShaderSourceFromRessource(_T("vertex.vert"));
  const GLchar* vertexShaderSource = strVertexShaderSource.c_str();
  glShaderSource(nVertexShader, 1, &vertexShaderSource, nullptr);
  glCompileShader(nVertexShader);
  glAttachShader(nShader, nVertexShader);
  GLuint nFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  std::string strFragmentShaderSource = GetShaderSourceFromRessource(_T("fragment.frag"));
  const GLchar* fragmentShaderSource = strFragmentShaderSource.c_str();
  glShaderSource(nFragmentShader, 1, &fragmentShaderSource, nullptr);
  glCompileShader(nFragmentShader);
  glAttachShader(nShader, nFragmentShader);
  glLinkProgram(nShader);

  wndData.programId = nShader;

  //#############################################################################
  //					Initialisationd de la cam�ra
  //#############################################################################

  GLfloat cameraAngle = wndData.m_fFovy;
  glm::vec3 vec3Up = glm::vec3{ 0.0f, 0.0f, 1.0f };
  glm::vec3 cameraPosition = glm::vec3{ 2.5f, 2.5f, 1.0f };
  glm::vec3 originPosition = glm::vec3{ 0.0f, 0.0f, 0.0f };

  glm::vec3 trPos = glm::vec3{ 0.0f };
  glm::vec3 trRot = glm::vec3{ 1.0f };
  glm::vec3 trScale = glm::vec3{ 1.0f };
  
  glm::mat4 ModelMatrix = glm::rotate(0.0f, trRot) * glm::translate(trPos) * glm::scale(trScale);
  glm::mat4 ViewMatrix = glm::lookAt(cameraPosition, originPosition, vec3Up);
  wndData.ProjMatrix = glm::perspective(glm::radians(cameraAngle), (float)nInitWndWidth / (float)nInitWndHeight, 0.01f, 1000.0f);
  glViewport(0, 0, nInitWndWidth, nInitWndHeight);

  GLint nModelMatrix = glGetUniformLocation(wndData.programId, "model");
  GLint nViewMatrix = glGetUniformLocation(wndData.programId, "view");
  wndData.nProjectionMatrix = glGetUniformLocation(wndData.programId, "proj");

  //#############################################################################
  //                    Multiplication de l'objet
  //#############################################################################

  glm::vec3 center[4] = {
	glm::vec3{ 1, 1, 0 },
	glm::vec3{ 1, -1, 0 },
	glm::vec3{ -1, 1, 0 },
	glm::vec3{ -1, -1, 0 }
  };

  GLuint nCenter[4];

  for (int i = 0; i < 4; ++i)
	  nCenter[i] = glGetUniformLocation(wndData.programId, "center");

  //#############################################################################
  //    R�cup�ration des identifiants des uniforms et initialise les valeurs
  //#############################################################################
  GLint nUniform_Phase = glGetUniformLocation(nShader, "phase");
  GLint nUniform_NbRipples = glGetUniformLocation(nShader, "nbRipples");
  GLint nUniform_ZScale = glGetUniformLocation(nShader, "zScale");
  wndData.unifNbRipples = nUniform_NbRipples;
  wndData.unifZScale = nUniform_ZScale;
  glUseProgram(nShader);
  glUniform1f(nUniform_NbRipples, 4.0f);
  glUniform1f(nUniform_ZScale, 0.01f);

  // Uniforme matrix de visualisation
  glUniformMatrix4fv(nModelMatrix, 1, GL_FALSE, glm::value_ptr(ModelMatrix));
  glUniformMatrix4fv(nViewMatrix, 1, GL_FALSE, glm::value_ptr(ViewMatrix));
  glUniformMatrix4fv(wndData.nProjectionMatrix, 1, GL_FALSE, glm::value_ptr(wndData.ProjMatrix));

  //#############################################################################
  //                          Boucle d'affichage
  //#############################################################################
  glEnable(GL_DEPTH_TEST);

  glfwSetTime(0);
  glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
  while(!glfwWindowShouldClose(pWnd))
  {
    glfwPollEvents();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLfloat t = (GLfloat)glfwGetTime();
    glUseProgram(nShader);
    glUniform1f(nUniform_Phase, t);

    //Affichage du mesh
    glBindVertexArray(wndData.VAO);
	
	glm::vec4 ctr;
	for (GLuint i = 0; i < 4; ++i)
	{
		/*ctr = ViewMatrix * glm::vec4(center[i], 1.0f);
		center[i] = glm::vec3(ctr) / ctr.w;*/
		 
		glUniform3fv(nCenter[i], 1, glm::value_ptr(center[i]));
		glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(wndData.polarMesh.GetIndicesBufferByteSize() / sizeof(GLuint)), GL_UNSIGNED_INT, nullptr);
	}
    glfwSwapBuffers(pWnd);
  }

  glfwTerminate();

  return 0;
}

void ScrollWheelCallback(GLFWwindow* pWnd, double xOffset, double yOffset)
{
	WndData& TheWndData = *reinterpret_cast<WndData*>(glfwGetWindowUserPointer(pWnd));

	std::cout << "beg : " << TheWndData.m_fFovy;
	TheWndData.m_fFovy += yOffset;

	if (TheWndData.m_fFovy < 0.0f)
		TheWndData.m_fFovy = (359.0f);
	else if (TheWndData.m_fFovy > 360.0f)
		TheWndData.m_fFovy = (1.0f);
	std::cout << ", end : " << TheWndData.m_fFovy << std::endl;

	TheWndData.ProjMatrix = glm::perspective(glm::radians(TheWndData.m_fFovy), (float)TheWndData.WndWidth / (float)TheWndData.WndHeight, 0.01f, 1000.0f);
	glUniformMatrix4fv(TheWndData.nProjectionMatrix, 1, GL_FALSE, glm::value_ptr(TheWndData.ProjMatrix));
}

void characterfunct(GLFWwindow* pWnd, unsigned int codepoint)
{
  WndData& TheWndData = *reinterpret_cast<WndData*>(glfwGetWindowUserPointer(pWnd));

  switch (codepoint)
  {
    //Fil de fer
  case 'f':
  case 'F':
  {
    static GLint mode = GL_FILL;
    if (mode == GL_LINE)
      mode = GL_FILL;
    else
      mode = GL_LINE;
    glPolygonMode(GL_FRONT_AND_BACK, mode);
    break;
  }

    //Echantillonage du rayon
  case 'r':
    TheWndData.polarMesh.SetNbRSteps(TheWndData.polarMesh.GetNbRSteps() + 1);
    break;
  case 'R':
    if (TheWndData.polarMesh.GetNbRSteps() > 2)
      TheWndData.polarMesh.SetNbRSteps(TheWndData.polarMesh.GetNbRSteps() - 1);
    break;

    //Nombre de cot�s du polygone principal
  case 'c':
    TheWndData.polarMesh.SetNbSides(TheWndData.polarMesh.GetNbSides() + 1);
    break;
  case 'C':
    if (TheWndData.polarMesh.GetNbSides() > 3)
      TheWndData.polarMesh.SetNbSides(TheWndData.polarMesh.GetNbSides() - 1);
    break;

    //Echelle en z
  case 'z':
  {
    glUseProgram(TheWndData.programId);
    float zScale;
    glGetUniformfv(TheWndData.programId, TheWndData.unifZScale, &zScale);
    zScale += 0.01f;
    glUniform1f(TheWndData.unifZScale, zScale);
  }
  break;
  case 'Z':
  {
    glUseProgram(TheWndData.programId);
    float zScale;
    glGetUniformfv(TheWndData.programId, TheWndData.unifZScale, &zScale);
    zScale -= 0.01f;
    if (zScale < 0.0f) zScale = 0.0f;
    glUniform1f(TheWndData.unifZScale, zScale);
  }
  break;

  //Nombre de vagues
  case 'v':
  {
    glUseProgram(TheWndData.programId);
    float nbRipples;
    glGetUniformfv(TheWndData.programId, TheWndData.unifNbRipples, &nbRipples);
    nbRipples += 0.1f;
    glUniform1f(TheWndData.unifNbRipples, nbRipples);
  }
  break;
  case 'V':
  {
    glUseProgram(TheWndData.programId);
    float nbRipples;
    glGetUniformfv(TheWndData.programId, TheWndData.unifNbRipples, &nbRipples);
    nbRipples -= 0.1f;
    if (nbRipples < 0.0f) nbRipples = 0.0f;
    glUniform1f(TheWndData.unifNbRipples, nbRipples);
  }
  break;

  }
  switch (codepoint)
  {
  case 'r':
  case 'R':
  case 'c':
  case 'C':
    glBindVertexArray(TheWndData.VAO);
    glBindBuffer(GL_ARRAY_BUFFER, TheWndData.VBO);
    glBufferData(GL_ARRAY_BUFFER, TheWndData.polarMesh.GetPolarVerticesBufferByteSize(), TheWndData.polarMesh.GetPolarVertices(), GL_STREAM_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, TheWndData.EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, TheWndData.polarMesh.GetIndicesBufferByteSize(), TheWndData.polarMesh.GetIndices(), GL_STREAM_DRAW);
    break;
  default:
    break;
  }

}

void sizefunct(GLFWwindow* pWnd, int width, int height)
{
	if (width * height == 0)
		return;
	WndData& TheWndData = *(reinterpret_cast<WndData*>(glfwGetWindowUserPointer(pWnd)));

	glViewport(0, 0, width, height);

	TheWndData.ProjMatrix = glm::perspective(glm::radians(TheWndData.m_fFovy), (float)TheWndData.WndWidth / (float)TheWndData.WndHeight, 0.01f, 1000.0f);
	TheWndData.nProjectionMatrix = glGetUniformLocation(TheWndData.programId, "proj");
	glUseProgram(TheWndData.programId);
	glUniformMatrix4fv(TheWndData.nProjectionMatrix, 1, GL_FALSE, glm::value_ptr(TheWndData.ProjMatrix));
}

