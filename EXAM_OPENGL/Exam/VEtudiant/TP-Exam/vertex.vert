#version 330 core
#define pi 3.1415926535897932384626433832795
layout (location = 0) in vec2 posPol2D;


uniform float phase;
uniform float nbRipples;
uniform float zScale;

float computeZ(float r, out vec2 normal, out float z_normalized);

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

uniform vec3 center;

void main()
{
	mat4 ctr = mat4(1);
	ctr[3].xyz = center;

    vec3 position;
    vec2 normal_rz;
    float z_n;
    position.x = posPol2D.x*cos(posPol2D.y);
    position.y = posPol2D.x*sin(posPol2D.y);
    position.z = computeZ(posPol2D.x, normal_rz, z_n);

    vec3 normal;
    normal.x = normal_rz.x*cos(posPol2D.y);
    normal.y = normal_rz.x*sin(posPol2D.y);
    normal.z = normal_rz.y;
    gl_Position = proj * model * view  * ctr * vec4(position, 1.0);
}

float computeZ(float r, out vec2 normal, out float z_normalized)
{
    float a = phase; 
    float b = nbRipples; 

    float w = 2.0 * pi * (a - 2.0 * b * r * r / (r + 1.0));
    float dw_num = 4.0 * pi * b * r * r + 8.0 * pi * b * r;
    float dw_den = r * r + 2.0 * r + 1.0;
    float dw = -dw_num / dw_den;

    float onde = cos(w);
    float donde = -dw * sin(w);

    float env = zScale * (cos(r * pi) / 2.0 + 0.5);
    float denv = zScale * (-pi * sin(r * pi) / 2.0);

    float z = env * onde;

    z_normalized = (z / zScale + 1.0) / 2.0;

    float t = -zScale * 0.5 * (pi * sin(r * pi) * cos(w) + (cos(r * pi) + 1.0) * sin(w) * dw);

    normal.x = -sin(atan(t));
    normal.y = cos(atan(t));
    
    return z;
}
