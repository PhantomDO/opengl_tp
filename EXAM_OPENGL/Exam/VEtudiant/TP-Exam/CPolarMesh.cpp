#include "stdafx.h"
#include "CPolarMesh.h"

constexpr float pi = 3.1415926535897932384626433832795f;

void CPolarMesh::refreshVBO()
{
  m_nNbVertices = 1ull + m_nNbSides * (m_nNbRSteps - 1ull) * (m_nNbRSteps) / 2ull;
  m_nNbTriangles = m_nNbSides * (m_nNbRSteps - 1ull) * (m_nNbRSteps - 1ull);
  m_fDeltaAnglePrincipal = 2.0f * pi / m_nNbSides;

  m_pVertices.resize(m_nNbVertices * 2);
  float* pCurVertex = m_pVertices.data();
  *pCurVertex++ = 0;
  *pCurVertex++ = 0;
  for (size_t k = 1; k < m_nNbVertices; ++k)
  {
    float& r = *(pCurVertex++);
    float& teta = *(pCurVertex++);

    auto det = (m_nNbSides / 4.0f - 2.0f + 2.0f * k) * m_nNbSides;
    auto step = static_cast<size_t>(0.5 + sqrt(det) / m_nNbSides);
    auto k_rel = k - 1ull + m_nNbSides * (step - (step * step + step) / 2ull);
    auto ratio = static_cast<float>((k_rel % step)) / step;

    auto icote = k_rel / step;
    auto fAnglePrincipal = icote * m_fDeltaAnglePrincipal;

    r = step / (m_nNbRSteps - 1.0f);
    teta = fAnglePrincipal + ratio * m_fDeltaAnglePrincipal;
  }

  m_pIndices.resize(m_nNbTriangles * 3);
  unsigned int* pT = m_pIndices.data();
  //faire les premiers � part ?
  for (unsigned int k = 0; k < m_nNbSides; ++k)
  {
    *pT++ = 0;
    *pT++ = k + 1;
    *pT++ = k + 2;
  }
  *(pT - 1) = 1;
  for (size_t k = m_nNbSides; k < m_nNbTriangles; ++k)
  {
    auto step = static_cast<size_t>(sqrt(static_cast<double>(k) / m_nNbSides) + 1.0);
    auto k_rel_triangle = k - m_nNbSides * (step - 1ull) * (step - 1ull);
    auto nbtriparcote = 2ull * step - 1ull;
    auto k_rel_cote = k_rel_triangle % nbtriparcote;
    auto id_ptDebutStep = 1 + m_nNbSides * step * (step - 1ull) / 2ull;
    auto id_ptDebutPreviousStep = (1ull + m_nNbSides * (step - 2ull) * (step - 1ull) / 2ull);
    auto icote = k_rel_triangle / nbtriparcote;
    auto id_ptDebutCot�Prev = id_ptDebutPreviousStep + icote * (step - 1);
    auto id_ptDebutCot� = id_ptDebutStep + icote * step;
    auto id_ptDebutNextStep = 1ull + m_nNbSides * step * (step + 1ull) / 2ull;

    auto temp = id_ptDebutCot�Prev + k_rel_cote / 2ull;
    *pT++ = static_cast<unsigned int>(temp == id_ptDebutStep ? id_ptDebutPreviousStep : temp);
    *pT++ = static_cast<unsigned int>(id_ptDebutCot� + (k_rel_cote + 1ull) / 2ull);

    if (k_rel_cote % 2)
    {
      temp = id_ptDebutCot�Prev + (k_rel_cote + 1ull) / 2ull;
      *pT++ = static_cast<unsigned int>(temp == id_ptDebutStep ? id_ptDebutPreviousStep : temp);
    }
    else
    {
      temp = id_ptDebutCot� + (k_rel_cote + 2ull) / 2ull;
      *pT++ = static_cast<unsigned int>(temp == id_ptDebutNextStep ? id_ptDebutStep : temp);
    }
  }
}
