#pragma once
#ifdef WIN32
#include <Windows.h>
#include <tchar.h>
#else
#define _T(x) (x)
#endif
#include <string>

const char* GetGLErrorString();
void PrintRendererInfo();
void terminate(const std::string& strErr);

#ifdef WIN32
std::string GetShaderSourceFromRessource(const std::wstring& filename);
#else
std::string GetShaderSourceFromRessource(const std::string& filename);
#endif

template<size_t val, size_t p>
struct pow_t {
  enum : size_t
  {
    pow = pow_t<val, p - 1ULL>::pow * val
  };
};

template<size_t val>
struct pow_t<val, 0ULL> {
  enum : size_t
  {
    pow = 1ULL
  };
};

