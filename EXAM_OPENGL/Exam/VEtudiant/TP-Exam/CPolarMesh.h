#pragma once
#include <vector>

class CPolarMesh
{
private:
  size_t m_nNbRSteps = 30;
  size_t m_nNbSides = 6;
  size_t m_nNbVertices;
  size_t m_nNbTriangles;
  float m_fDeltaAnglePrincipal;

  std::vector<float> m_pVertices;
  std::vector<unsigned int> m_pIndices;

  void refreshVBO();

public:
  CPolarMesh() { refreshVBO(); }
  void SetNbRSteps(size_t nNbRSteps) { m_nNbRSteps = nNbRSteps; refreshVBO(); }
  void SetNbSides(size_t nNbSides) { m_nNbSides = nNbSides; refreshVBO(); }

  size_t GetNbRSteps() const { return m_nNbRSteps; }
  size_t GetNbSides() const { return m_nNbSides; }
  size_t GetNbVertices() const { return m_nNbVertices; }
  size_t GetNbTriangles() const { return m_nNbTriangles; }
  float GetDeltaAnglePrincipal() const { return m_fDeltaAnglePrincipal; }

  const float* GetPolarVertices() const { return m_pVertices.data(); }
  const unsigned int* GetIndices() const { return m_pIndices.data(); }
  size_t GetPolarVerticesBufferByteSize() const { return m_pVertices.size() * sizeof(float); }
  size_t GetIndicesBufferByteSize() const { return m_pIndices.size() * sizeof(unsigned int); }
};

