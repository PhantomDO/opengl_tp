#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>

#include "Color.h"
#include <vector>
#include <math.h>

#include <Shader.h>
#include <ShaderProgram.h>
#include <ShaderException.h>


extern "C" _declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;

void CursorPosCallback(GLFWwindow* window, double xpos, double ypos);
void KeyDownCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void ShaderCompilationTest(GLint vertexShader);
void ProgramShaderLinkedTest(GLint program, GLint vertexShader, GLint fragmentShader);
std::string ReadShaderFile(const char* shader);

int main(int argc, char* argv[])
{
	// MINIMUM START

	glfwInit();
	if (!glfwInit()) exit(EXIT_FAILURE); //Fonction de la biblioth�que GLFW

	glGetString(GL_VERSION); //Fonction de la biblioth�que OpenGL du syst�me

	// Initialisation du fenetre opengl en version 3.
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Cr�e la fenetre et la rend contextuable pour opengl
	GLFWwindow* pWnd = glfwCreateWindow(600, 600, "Hello World", nullptr, nullptr);
	if (!pWnd)
	{
		std::cerr << "Impossible de cr�er la fenetre!" << std::endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(pWnd); 

	glewExperimental = GL_TRUE;
	glewInit(); //Fonction de la biblioth�que GLEW

	// MINIMUM END

	// COLOR
	CColor* backgrondColor = new CColor(0.33f, 1.0f, 1.0f);
	std::cout	<< backgrondColor->GetR() << ", "
				<< backgrondColor->GetG() << ", "
				<< backgrondColor->GetB() << std::endl;

	CColor* orangeColor = new CColor(0.39f, 1.0f, 1.0f);
	CColor* blueColor = new CColor(1.0f, 1.0f, 1.0f);
	
	glClearColor(
		backgrondColor->GetR(),
		backgrondColor->GetG(),
		backgrondColor->GetB(),
		1.0f
	);
								
	const GLuint sizeVertexRect = (2 * (2 + 3) * 360);
	const GLfloat PI = 3.1415926535897932384626433832795;
	const GLfloat radiusInt = 0.6f, radiusOut = 0.8f;

	GLfloat* verticesCircle = new GLfloat[sizeVertexRect];
	GLint indexVtx = 0;
	// For odd n, vertices n, n+1, and n+2 define triangle n. For even n, vertices n+1, n, and n+2 define triangle n.
	// indice 1 fosi sur 2 : 0,1,2 ;snon : 2,1,3
	// donc indice paire : n, n+1, n+2; impaire : n+1, n, n+2

	for (GLint i = 0; i <= 360; i++)
	{
		GLfloat angle = (i) * (PI / 180.0f);		
		GLfloat intX = radiusInt * std::cos(angle);
		GLfloat intY = radiusInt * std::sin(angle);
		GLfloat outX = radiusOut * std::cos(angle);
		GLfloat outY = radiusOut * std::sin(angle);
		CColor color{ static_cast<GLfloat>(i % 360), 1.0, 1.0 };
		
		// INTER POS
		verticesCircle[indexVtx++] = intX;	/* -X */
		verticesCircle[indexVtx++] = intY;	/* -Y */
		// INTER COL
		verticesCircle[indexVtx++] = color.GetR();	/* -R */
		verticesCircle[indexVtx++] = color.GetG();	/* -G */
		verticesCircle[indexVtx++] = color.GetB();	/* -B */
		// EXTER POS
		verticesCircle[indexVtx++] = outX;	/* X */
		verticesCircle[indexVtx++] = outY;	/* Y */
		// EXTER COL
		verticesCircle[indexVtx++] = color.GetR();	/* R */
		verticesCircle[indexVtx++] = color.GetG();	/* G */
		verticesCircle[indexVtx++] = color.GetB();	/* B */
	}

	GLuint VAO, VBO;
	// VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	// VBO
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeVertexRect * sizeof(GLfloat), &verticesCircle[0], GL_STATIC_DRAW);

	// Position
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(0));
	glEnableVertexAttribArray(0);
	// Couleurs
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	//delete[] indicesCircle;

	CShaderProgram shaderProgram{
		CShader { GL_VERTEX_SHADER, std::ifstream{ "triangleVertex.glsl" } },
		CShader { GL_FRAGMENT_SHADER, std::ifstream{ "triangleFragment.glsl" } }
	};
	shaderProgram.Use();

	// pointeur sur les touche du claver
	glfwSetKeyCallback(pWnd, KeyDownCallback);
	// pointeur sur la couleur de la fenetre
	glfwSetWindowUserPointer(pWnd, backgrondColor);
	// pointeur sur la position de la souris
	glfwSetCursorPosCallback(pWnd, CursorPosCallback);

	while (!glfwWindowShouldClose(pWnd))
	{
		glfwPollEvents();

		// change la couleur de la fenetre
		glClearColor(backgrondColor->GetR(), backgrondColor->GetG(), backgrondColor->GetB(), 1.0f);
		// remet la couleur par default
		glClear(GL_COLOR_BUFFER_BIT);

		// affiche le triangle depuis le vbo
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 360*5);
		// affiche ls triangles rempli
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glfwSwapBuffers(pWnd);
	}

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glfwTerminate();
	return 0;
}

void CursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	int windowSizeX, windowSizeY;
	CColor& color = *(reinterpret_cast<CColor*>(glfwGetWindowUserPointer(window)));

	//std::cout << xpos << ", " << ypos << std::endl;

	// r�cup�re taille de la fenetre
	glfwGetWindowSize(window, &windowSizeX, &windowSizeY);
	
	color.SetHSV(color.GetH(), static_cast<float>(xpos/windowSizeX), static_cast<float>(ypos/windowSizeY));
}

void KeyDownCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
		return;

	CColor& color = *(reinterpret_cast<CColor*>(glfwGetWindowUserPointer(window)));
	float hValue = color.GetH();

	switch (key)
	{
	case GLFW_KEY_DOWN:
		hValue -= 1.0f;
		if (hValue < 0.0f)
			hValue = 359.0f;
		color.SetHSV(hValue, color.GetS(), color.GetV());
		break;
	case GLFW_KEY_UP:
		hValue += 1.0f;
		if (hValue > 360.0f)
			hValue = 0.0f;
		color.SetHSV(hValue, color.GetS(), color.GetV());
		break;
	case GLFW_KEY_W:
		break;
	default:
		break;
	}
}

void ShaderCompilationTest(GLint vertexShader)
{
	GLint isCompiled;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);

		for (auto& i : infoLog)
			std::cout << i;

		return;
	}
}

void ProgramShaderLinkedTest(GLint program, GLint vertexShader, GLint fragmentShader)
{
	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

		// We don't need the program anymore.
//		glDeleteProgram(program);
		// Don't leak shaders either.
//		glDeleteShader(vertexShader);
//		glDeleteShader(fragmentShader);
		
		// Use the infoLog as you see fit.

		// In this simple program, we'll just leave
		return;
	}
}

std::string ReadShaderFile(const char* shader)
{
	// lis le fichier 
	std::ifstream file(shader);
	if (!file) return std::string();

	size_t size = file.gcount();

	// remet les caract�re au d�part du fichier
	file.clear();
	file.seekg(0, std::ios_base::beg);

	// copie les caract�re dans le string stream
	std::stringstream sstr;
	sstr << file.rdbuf();
	file.close();

	// renvoie une string du string stream pass�
	return sstr.str();
}
