#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class CTransform
{
private:
	glm::vec3 m_Position;
	glm::vec3 m_Rotation;
	glm::vec3 m_Scale;

public:
	CTransform();
	~CTransform();

	CTransform(glm::vec3 pos, glm::vec3 rot, glm::vec3 scale);

	glm::vec3& GetPosition() { return m_Position; }
	glm::vec3& GetRotation() { return m_Rotation; }
	glm::vec3& GetScale() { return m_Scale; }

	void SetPosition(GLfloat x, GLfloat y, GLfloat z);
	void SetRotation(GLfloat x, GLfloat y, GLfloat z);
	void SetScale(GLfloat x, GLfloat y, GLfloat z);
};