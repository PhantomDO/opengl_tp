#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Transform.h"
#include "Camera.h"

class CCoreEngine
{
private:
	bool m_FillView;
	GLfloat m_TextureBlank;
	GLfloat m_TextureAge;
	GLfloat m_BoundingSize;
	CCamera m_Camera;
	CTransform m_Transform;

	struct MVPMatrix
	{
		glm::mat4 ModelMatrix;
		glm::mat4 ViewMatrix;
		glm::mat4 ProjectionMatrix;
	} m_mvpMatrix;

	struct UMatrix
	{
		GLint nModelMatrix;
		GLint nViewMatrix;
		GLint nProjectionMatrix;
	} m_uMatrix;

public:
	CCoreEngine();
	~CCoreEngine();

	bool& GetFillView() { return m_FillView; }
	GLfloat& GetTextureBlank() { return m_TextureBlank; }
	GLfloat& GetTextureAge() { return m_TextureAge; }
	GLfloat& GetBoundingSize() { return m_BoundingSize; }

	CCamera& GetCamera() { return m_Camera; }
	CTransform& GetTransform() { return m_Transform; }
	MVPMatrix& GetMatrixs() { return m_mvpMatrix; }
	UMatrix& GetUniformMatrixs() { return m_uMatrix; }

	void SetFillView(bool value) { m_FillView = value; }
	void SetTextureBlank(GLfloat value) { m_TextureBlank = value; }
	void SetTextureAge(GLfloat value);
	void SetBoudingSize(GLfloat size) { m_BoundingSize = size; }

	void SetCamera(GLfloat angle, glm::vec3 pos);
	void SetCamera(GLfloat angle, glm::vec3 upVec, glm::vec3 pos);
	void SetTransform(glm::vec3 pos, glm::vec3 rot, glm::vec3 scale);
	void SetMatrixs(glm::mat4 model, glm::mat4 view, glm::mat4 proj);
	void SetUniformMatrixs(GLint model, GLint view, GLint proj);
};