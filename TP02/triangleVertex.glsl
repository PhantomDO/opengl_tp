#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 inputColor;
layout (location = 3) in vec2 inputTexCoord;

out vec4 color;
out vec2 texCoord;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main() 
{
	gl_Position = proj * view * model * vec4(position.x, position.y, position.z, 1.0);
	color = vec4(inputColor.r, inputColor.g, inputColor.b, 1.0);
	texCoord = inputTexCoord;
}