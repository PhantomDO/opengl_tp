#include "Transform.h"

CTransform::CTransform()
{
	SetPosition(0.0f, 0.0f, 0.0f);
	SetRotation(0.0f, 0.0f, 0.0f);
	SetScale(0.0f, 0.0f, 0.0f);
}

CTransform::~CTransform()
{
}

CTransform::CTransform(glm::vec3 pos, glm::vec3 rot, glm::vec3 scale)
{
	SetPosition(pos.x, pos.y, pos.z);
	SetRotation(rot.x, rot.y, rot.z);
	SetScale(scale.x, scale.y, scale.z);
}

void CTransform::SetPosition(GLfloat x, GLfloat y, GLfloat z)
{
	m_Position = glm::vec3{ x, y, z };
}

void CTransform::SetRotation(GLfloat x, GLfloat y, GLfloat z)
{
	m_Rotation = glm::vec3{ x, y, z };
}

void CTransform::SetScale(GLfloat x, GLfloat y, GLfloat z)
{
	m_Scale = glm::vec3{ x, y, z };
}
