#version 330 core

in vec4 color;
in vec2 texCoord;
out vec4 fragColor;

uniform sampler2D material;
uniform sampler2D material_old;
uniform float texBlank;
uniform float fAge;

void main() 
{
	// fragColor = mix(texture(material, texCoord), vec4(1.0), texBlank); // TP2
	vec4 mat = texture(material, texCoord);
	vec4 old = texture(material_old, texCoord);
	fragColor = ((1.0 - old.a * fAge) * mat.rgba) + ((old.a * fAge) * old.rgba);
}