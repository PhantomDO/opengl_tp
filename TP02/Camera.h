#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Transform.h"

class CCamera
{
private:
	GLfloat m_CameraAngle;
	glm::vec3 m_Vector3Up;
	CTransform m_CameraTransform;
public:
	CCamera();
	~CCamera();

	CCamera(GLfloat angle, glm::vec3 pos);
	CCamera(GLfloat angle, glm::vec3 upVec, glm::vec3 pos);

	GLfloat& GetAngle() { return m_CameraAngle; }
	glm::vec3& GetVec3Up() { return m_Vector3Up; }
	CTransform& GetTransform() { return m_CameraTransform; }

	void SetAngle(GLfloat angle) { m_CameraAngle = angle; }
	void SetVec3Up(GLfloat x, GLfloat y, GLfloat z) { m_Vector3Up = glm::vec3{ x, y, z }; }
	void SetTransform(CTransform tr) { m_CameraTransform = tr; }
};