// TP02.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>

#include "Color.h"
#include <vector>
#include <math.h>

#include "MeshLoader.h"
#include <Shader.h>
#include <ShaderProgram.h>
#include <ShaderException.h>
#include "CoreEngine.h"

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <FreeImagePlus.h>

void KeyDownCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void ScrollWheelCallback(GLFWwindow* window, double xOffset, double yOffset);
void TurnAround(GLFWwindow* window, GLfloat value);

extern "C" _declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;

#define WIDTH 800
#define HEIGHT 600

int main()
{
	// MINIMUM START

	glfwInit();
	if (!glfwInit()) exit(EXIT_FAILURE); //Fonction de la biblioth�que GLFW

	glGetString(GL_VERSION); //Fonction de la biblioth�que OpenGL du syst�me

	// Initialisation du fenetre opengl en version 3.
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Cr�e la fenetre et la rend contextuable pour opengl
	GLFWwindow* pWnd = glfwCreateWindow(WIDTH, HEIGHT, "Hello World", nullptr, nullptr);
	if (!pWnd)
	{
		std::cerr << "Impossible de cr�er la fenetre!" << std::endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(pWnd);

	glewExperimental = GL_TRUE;
	glewInit(); //Fonction de la biblioth�que GLEW

	//CColor* backgrondColor = new CColor(1.0f, 1.0f, 1.0f);
	// MINIMUM END


	/* LOAD FILE */
	const std::string filePath = "knot.mesh";
	CMeshLoader loader;
	loader.loadFromFile(filePath);

	/* LOAD TEXTURE */
	const int nNbTexture = 2;
	fipImage textureImg[nNbTexture];
	const char* texturePath[nNbTexture] = { "MtlPlat2.jpg", "MtlPlat2-old.png" };
	unsigned char* texPixels[nNbTexture];

	GLuint texWidth[nNbTexture];
	GLuint texHeight[nNbTexture];
	GLuint texWidthPow2[nNbTexture];
	GLuint texHeightPow2[nNbTexture];

	GLuint textureGL[nNbTexture];
	glGenTextures(2, textureGL);

	for (size_t i = 0; i < nNbTexture; i++)
	{
		textureImg[i].load(texturePath[i]);
		textureImg[i].convertTo32Bits();

		texWidth[i] = textureImg[i].getWidth();
		texHeight[i] = textureImg[i].getHeight();

		texPixels[i] = textureImg[i].accessPixels();

		texWidthPow2[i] = 1 << static_cast<GLuint>(std::ceil(log2(static_cast<double>(texWidth[i]))));
		texHeightPow2[i] = 1 << static_cast<GLuint>(std::ceil(log2(static_cast<double>(texHeight[i]))));

		textureImg[i].rescale(texWidthPow2[i], texHeightPow2[i], FILTER_BICUBIC);

		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, textureGL[i]);
		glTexImage2D(
			GL_TEXTURE_2D, 0, GL_RGBA,
			textureImg[i].getWidth(), textureImg[i].getHeight(), 0,
			GL_BGRA, GL_UNSIGNED_BYTE, textureImg[i].accessPixels()
		);

		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}


	GLuint VAO, VBO, EBO;
	// VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	// VBO
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, loader.getVBOSize() * sizeof(GLfloat), loader.getVBO(), GL_STATIC_DRAW);
	// EBO
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, loader.getEBOSize() * sizeof(GLuint), loader.getEBO(), GL_STATIC_DRAW);
	
	// Position
	glVertexAttribPointer(0, loader.getNbPositionComponents(), GL_FLOAT, GL_FALSE, 
		loader.getStride() * sizeof(GLfloat), reinterpret_cast<GLvoid*>(0));
	glEnableVertexAttribArray(0);
	// Normal
	glVertexAttribPointer(1, loader.getNbNormalComponents(), GL_FLOAT, GL_FALSE, 
		loader.getStride() * sizeof(GLfloat), reinterpret_cast<GLvoid*>(loader.getNormalOffset() * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	// Color
	glVertexAttribPointer(2, loader.getNbColorComponents(), GL_FLOAT, GL_FALSE, 
		loader.getStride() * sizeof(GLfloat), reinterpret_cast<GLvoid*>(loader.getColorOffset() * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	// TexCoord
	glVertexAttribPointer(3, loader.getNbTexCoordComponents(), GL_FLOAT, GL_FALSE, 
		loader.getStride() * sizeof(GLfloat), reinterpret_cast<GLvoid*>(loader.getTexCoordOffset() * sizeof(GLfloat)));
	glEnableVertexAttribArray(3);

	CShaderProgram shaderProgram{
		CShader { GL_VERTEX_SHADER, std::ifstream{ "triangleVertex.glsl" } },
		CShader { GL_FRAGMENT_SHADER, std::ifstream{ "triangleFragment.glsl" } }
	};

	CCoreEngine* core = new CCoreEngine;

	core->SetBoudingSize(glm::distance(
		glm::vec3{ loader.getXmin(), loader.getYmin(), loader.getZmin() },
		glm::vec3{ loader.getXmax(), loader.getYmax(), loader.getZmax() }
	));

	core->SetCamera(
		45.0f,
		glm::vec3{ 0.0f, 1.0f, 0.0f },
		glm::vec3{ 0.0f, 0.0f, core->GetBoundingSize() }
	);
	CCamera& cam = core->GetCamera();

	core->SetTransform(
		glm::vec3{ 0.0f, 0.0f, 0.0f },
		glm::vec3{ 1.0f },
		glm::vec3{ 1.0f }
	);
	CTransform& tr = core->GetTransform();

	core->SetMatrixs(
		glm::rotate(0.0f, tr.GetRotation())* glm::translate(tr.GetPosition())* glm::scale(tr.GetScale()),
		glm::lookAt(cam.GetTransform().GetPosition(), tr.GetPosition(), cam.GetVec3Up()),
		glm::perspective(glm::radians(cam.GetAngle()), (float)WIDTH / (float)HEIGHT, 0.01f, 1000.0f)
	);
	glViewport(0, 0, WIDTH, HEIGHT);

	core->SetUniformMatrixs(
		glGetUniformLocation(shaderProgram.GetProgramId(), "model"),
		glGetUniformLocation(shaderProgram.GetProgramId(), "view"),
		glGetUniformLocation(shaderProgram.GetProgramId(), "proj")
	);

	GLuint nTexBlank = glGetUniformLocation(shaderProgram.GetProgramId(), "texBlank");

	shaderProgram.Use();

	glUniformMatrix4fv(core->GetUniformMatrixs().nModelMatrix, 1, GL_FALSE, 
		glm::value_ptr(core->GetMatrixs().ModelMatrix));
	glUniformMatrix4fv(core->GetUniformMatrixs().nViewMatrix, 1, GL_FALSE,
		glm::value_ptr(core->GetMatrixs().ViewMatrix));
	glUniformMatrix4fv(core->GetUniformMatrixs().nProjectionMatrix, 1, GL_FALSE,
		glm::value_ptr(core->GetMatrixs().ProjectionMatrix));

	// Associe les unité de texture
	glUniform1i(glGetUniformLocation(shaderProgram.GetProgramId(), "material"), 0);
	glUniform1i(glGetUniformLocation(shaderProgram.GetProgramId(), "material_old"), 1);

	// pointeur sur la camera de la fenetre
	glfwSetWindowUserPointer(pWnd, core);
	// pointeur sur la mouse wheel
	glfwSetScrollCallback(pWnd, ScrollWheelCallback);
	// pointeur sur les touche du claver
	glfwSetKeyCallback(pWnd, KeyDownCallback);
	// activation du z-buffer
	glEnable(GL_DEPTH_TEST);

	GLfloat oldTimeSinceStart = 0.0f;
	GLfloat inc = 0.0f;

	while (!glfwWindowShouldClose(pWnd))
	{
		GLfloat timeSinceStart = glfwGetTime();
		GLfloat deltaTime = timeSinceStart - oldTimeSinceStart;
		oldTimeSinceStart = timeSinceStart;

		glfwPollEvents();
		// change la couleur de la fenetre
		//glClearColor(backgrondColor->GetR(), backgrondColor->GetG(), backgrondColor->GetB(), 1.0f);
		// remet la couleur par default
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		inc += 0.1f;

		if (inc > 359.9f)
			inc = 0.1f;

		TurnAround(pWnd, inc);

		glDrawElements(loader.getPrimitivesType(), loader.getEBOSize(), GL_UNSIGNED_INT, (GLvoid*)(0));

		glUniform1f(nTexBlank, core->GetTextureBlank());
		glUniform1f(glGetUniformLocation(shaderProgram.GetProgramId(), "fAge"), core->GetTextureAge());

		// affiche ls triangles rempli
		if (!core->GetFillView())
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		glfwSwapBuffers(pWnd);
	}

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);

	glDeleteTextures(2, textureGL);

	glfwTerminate();
	return 0;
}


void KeyDownCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
		return;

	CCoreEngine& core = *(reinterpret_cast<CCoreEngine*>(glfwGetWindowUserPointer(window)));
	CCamera& cam = core.GetCamera();
	CTransform& tr = core.GetTransform();
	glm::vec3& camPos = cam.GetTransform().GetPosition();
	GLfloat v = (core.GetTextureBlank() <= 0) ? 1.0f : 0.0f;
	GLfloat& age = core.GetTextureAge();

	switch (key)
	{
	case GLFW_KEY_T:
		core.SetTextureBlank(v);
		break;
	case GLFW_KEY_W:
		core.SetFillView(!core.GetFillView());
		break;
	case GLFW_KEY_V:
	{
		if (mods & GLFW_MOD_SHIFT)
			core.SetTextureAge(age + 0.01f);
		else
			core.SetTextureAge(age - 0.01f);
		break;
	}
	// CAMERA MOVEMENT
	case GLFW_KEY_DOWN:
		camPos = cam.GetTransform().GetPosition();
		cam.GetTransform().SetPosition(camPos.x, camPos.y - 1.0f, camPos.z);
		core.GetMatrixs().ViewMatrix = glm::lookAt(cam.GetTransform().GetPosition(), tr.GetPosition(), cam.GetVec3Up());
		glUniformMatrix4fv(core.GetUniformMatrixs().nViewMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ViewMatrix));
		break;
	case GLFW_KEY_UP:
		camPos = cam.GetTransform().GetPosition();
		cam.GetTransform().SetPosition(camPos.x, camPos.y + 1.0f, camPos.z);
		core.GetMatrixs().ViewMatrix = glm::lookAt(cam.GetTransform().GetPosition(), tr.GetPosition(), cam.GetVec3Up());
		glUniformMatrix4fv(core.GetUniformMatrixs().nViewMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ViewMatrix));
		break;
	case GLFW_KEY_LEFT:
		camPos = cam.GetTransform().GetPosition();
		cam.GetTransform().SetPosition(camPos.x - 1.0f, camPos.y, camPos.z);
		core.GetMatrixs().ViewMatrix = glm::lookAt(cam.GetTransform().GetPosition(), tr.GetPosition(), cam.GetVec3Up());
		glUniformMatrix4fv(core.GetUniformMatrixs().nViewMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ViewMatrix));
		break;
	case GLFW_KEY_RIGHT:
		camPos = cam.GetTransform().GetPosition();
		cam.GetTransform().SetPosition(camPos.x + 1.0f, camPos.y, camPos.z);
		core.GetMatrixs().ViewMatrix = glm::lookAt(cam.GetTransform().GetPosition(), tr.GetPosition(), cam.GetVec3Up());
		glUniformMatrix4fv(core.GetUniformMatrixs().nViewMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ViewMatrix));
		break;
	case GLFW_KEY_S:
		camPos = cam.GetTransform().GetPosition();
		cam.GetTransform().SetPosition(camPos.x, camPos.y, camPos.z - 1.0f);
		core.GetMatrixs().ViewMatrix = glm::lookAt(cam.GetTransform().GetPosition(), tr.GetPosition(), cam.GetVec3Up());
		glUniformMatrix4fv(core.GetUniformMatrixs().nViewMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ViewMatrix));
		break;
	case GLFW_KEY_Z:
		camPos = cam.GetTransform().GetPosition();
		cam.GetTransform().SetPosition(camPos.x, camPos.y, camPos.z + 1.0f);
		core.GetMatrixs().ViewMatrix = glm::lookAt(cam.GetTransform().GetPosition(), tr.GetPosition(), cam.GetVec3Up());
		glUniformMatrix4fv(core.GetUniformMatrixs().nViewMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ViewMatrix));
		break;
	default:
		break;
	}
}

void ScrollWheelCallback(GLFWwindow* window, double xOffset, double yOffset)
{	
	CCoreEngine& core = *(reinterpret_cast<CCoreEngine*>(glfwGetWindowUserPointer(window)));
	CCamera& cam = core.GetCamera();


	std::cout << "beg : " << cam.GetAngle();
	cam.SetAngle(cam.GetAngle() + yOffset);

	if (cam.GetAngle() < 0.0f)
		cam.SetAngle(359.0f);
	else if (cam.GetAngle() > 360.0f)
		cam.SetAngle(1.0f);
	std::cout << ", end : " << cam.GetAngle() << std::endl;

	core.GetMatrixs().ProjectionMatrix = glm::perspective(glm::radians(cam.GetAngle()), (float)WIDTH / (float)HEIGHT, 0.01f, 1000.0f);
	glUniformMatrix4fv(core.GetUniformMatrixs().nProjectionMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ProjectionMatrix));
}

void TurnAround(GLFWwindow* window, GLfloat value)
{
	CCoreEngine& core = *(reinterpret_cast<CCoreEngine*>(glfwGetWindowUserPointer(window)));
	CTransform& tr = core.GetTransform();

	core.GetMatrixs().ModelMatrix = 
		glm::rotate(glm::radians(value), glm::vec3{ 0.0f, tr.GetRotation().y, 0.0f }) *
		glm::translate(tr.GetPosition()) *
		glm::scale(tr.GetScale());

	glUniformMatrix4fv(core.GetUniformMatrixs().nModelMatrix, 1, GL_FALSE, glm::value_ptr(core.GetMatrixs().ModelMatrix));
}
// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
