#include "Camera.h"

CCamera::CCamera()
{
}

CCamera::~CCamera()
{
}

CCamera::CCamera(GLfloat angle, glm::vec3 pos)
{
	SetAngle(angle);
	SetVec3Up(0.0f, 1.0f, 0.0f);
	SetTransform(
		CTransform{
			pos,
			glm::vec3{ 0.0f },
			glm::vec3{ 0.0f }
		}
	);
}

CCamera::CCamera(GLfloat angle, glm::vec3 upVec, glm::vec3 pos)
{
	SetAngle(angle);
	SetVec3Up(upVec.x, upVec.y, upVec.z);
	SetTransform(
		CTransform{
			pos,
			glm::vec3{ 0.0f },
			glm::vec3{ 0.0f }
		}
	);
}
