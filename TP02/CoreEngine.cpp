#include "CoreEngine.h"

CCoreEngine::CCoreEngine()
{
	m_FillView = true;
	m_TextureBlank = 0.0f;
	m_TextureAge = 1.0f;
	m_BoundingSize = 0.0f;
	SetCamera(0, glm::vec3{ 0.0f });
	SetTransform(glm::vec3{ 0.0f }, glm::vec3{ 0.0f }, glm::vec3{ 1.0f });
	SetMatrixs(glm::mat4{ 0.0f }, glm::mat4{ 0.0f }, glm::mat4{ 0.0f });
	SetUniformMatrixs(0, 0, 0);
}

CCoreEngine::~CCoreEngine()
{
}

void CCoreEngine::SetTextureAge(GLfloat value)
{ 
	if (value > 0.99f)
		m_TextureAge = 1.0f;
	else if (value < 0.01f)
		m_TextureAge = 0.0f;
	else
		m_TextureAge = value;
}

void CCoreEngine::SetCamera(GLfloat angle, glm::vec3 pos)
{
	m_Camera = CCamera{ angle, pos };
}

void CCoreEngine::SetCamera(GLfloat angle, glm::vec3 upVec, glm::vec3 pos)
{
	m_Camera = CCamera{ angle, upVec, pos };
}

void CCoreEngine::SetTransform(glm::vec3 pos, glm::vec3 rot, glm::vec3 scale)
{
	m_Transform = CTransform{ pos, rot, scale };
}

void CCoreEngine::SetMatrixs(glm::mat4 model, glm::mat4 view, glm::mat4 proj)
{
	m_mvpMatrix.ModelMatrix = model;
	m_mvpMatrix.ViewMatrix = view;
	m_mvpMatrix.ProjectionMatrix = proj;
}

void CCoreEngine::SetUniformMatrixs(GLint model, GLint view, GLint proj)
{
	m_uMatrix.nModelMatrix = model;
	m_uMatrix.nViewMatrix = view;
	m_uMatrix.nProjectionMatrix = proj;
}
